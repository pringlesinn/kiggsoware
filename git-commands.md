$ git status
shows which files you have added, deleted or updated

$ git add .
adds every file to be committed

$ git add fullfilepath
adds only the specified path(you can see full file path when you run $ git status) to be committed. this step can be repeated multiple times until you add all files you want to commit.

$ git diff
checks all differences your edited files have since the last commit (in case there are too many, to get out of this, you may need to press q)

$ git diff full filepath
check the difference of that particular file with your last commit

$ git checkout fullfilepath
revert all changes you made on the specified file. good when you don't know why you editted the file and it is just too hard to revert the changes by hand

$ git reset --hard
when you have screwed everything up, don't know what to do and just want to go back to the last committed version you have

$ git branch
shows all branches you have created. the one with * is the one you are at the moment

$ git branch -a
shows all branches from your bitbucket for that particular project (useful when you are working with more people and need to check their work for some reason)

$ git commit -m "message"
commits your code with the message you chose

$ git commit --amend
opens vi with your last committed message to be updated. (useful when you want to change the commit message or committed with something else and want to change)

$ git log
checks every commit you had for the branch you are on

$ git checkout branchname
try to checkout(switch) the branch locally if you have it. if you don't it's gonna try to check out from your bitbucket. if it doesn't have there, it's gonna throw an error

$ git checkout -b branchname
creates a new branch locally

$ git push origin branchname
pushes the code from your branch to bitbucket, where branchname is the branch you are on

$ git branch -d branchname
removes branch from local branches (although he keeps asking us to do that, I do not recommend as it won't hurt anything. I only recommend when you did it with another purpose other than create a code to do something mandatory)

$ git pull
if there are changes in the server that you don't have, downloads it. this will only work if the branch has been pushed before to bitbucket, otherwise, it doesn't make sense

$ git merge branchname
gets the code from branchname and applies to the one you currently are on
<?php

use Illuminate\Database\Seeder;

class products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => 'Turbo Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => 'dishwasher',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'dishwasher.jpg',
	        'description' => 'This is dishwasher',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 3,
	        'name' => 'toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 4,
	        'name' => 'Fridge',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'lg-refrigerator.jpg',
	        'description' => 'This is fridge',
	        'is_active' => false
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 5,
	        'name' => 'dryer',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'dryer.jpg',
	        'description' => 'This is dryer',
	        'is_active' => false
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => '1 Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => '2 Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => '3 Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => '4 Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => '5 Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 1,
	        'name' => '6 Toaster',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Toaster',
	        'is_active' => true
        ]);

        DB::table('products')->insert([
	        'supplier_id' => 2,
	        'name' => 'Ninja Blender',
	        'price' => 1000,
	        'quantity' => 10,
	        'image' => 'toaster.jpg',
	        'description' => 'This is Blender',
	        'is_active' => true
        ]);
       
    }
}

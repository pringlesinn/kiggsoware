<?php

use Illuminate\Database\Seeder;

class transactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('transactions')->insert([
	        'purchase_order_id' => 1,
	        'response_json' => '{}',
	        'reference_number' => '4567887882'
	    ]);

    	DB::table('transactions')->insert([
	        'purchase_order_id' => 1,
	        'response_json' => '{}',
	        'reference_number' => '435672882'
	    ]);

    	DB::table('transactions')->insert([
	        'purchase_order_id' => 3,
	        'response_json' => '{}',
	        'reference_number' => '435345682'
	    ]);


    	DB::table('transactions')->insert([
	        'purchase_order_id' => 4,
	        'response_json' => '{}',
	        'reference_number' => '436728882'
	    ]);


    	DB::table('transactions')->insert([
	        'purchase_order_id' => 5,
	        'response_json' => '{}',
	        'reference_number' => '4747637882'
	    ]);


    }
}

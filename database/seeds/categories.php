<?php

use Illuminate\Database\Seeder;

class categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Appliances',
            'slug' => Str::slug('Appliances'),
            'is_active' => true
        ]);

        DB::table('categories')->insert([
            'name' => 'Knives',
            'slug' => Str::slug('Knives'),
            'is_active' => true
        ]);

        DB::table('categories')->insert([
            'name' => 'Cookware',
            'slug' => Str::slug('Cookware'),
            'is_active' => true
        ]);
        
        DB::table('categories')->insert([
            'name' => 'Homecooking',
            'slug' => Str::slug('Homecooking'),
            'is_active' => true
        ]);

        DB::table('categories')->insert([
            'name' => 'Sales',
            'slug' => Str::slug('Sales'),
            'is_active' => false
        ]);

        //Subcategories of Appliances
        DB::table('categories')->insert([
            'parent_category_id' => 1,
            'name' => 'Blender',
            'slug' => Str::slug('Blender'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 1,
            'name' => 'Food Processors',
            'slug' => Str::slug('Food Processors'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 1,
            'name' => 'Juicers',
            'slug' => Str::slug('Juicers'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 1,
            'name' => 'Mixers',
            'slug' => Str::slug('Mixers'),
            'is_active' => true
        ]);

        //Subcategories of Cookware
        DB::table('categories')->insert([
            'parent_category_id' => 3,
            'name' => 'Cookware Sets',
            'slug' => Str::slug('Cookware Sets'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 3,
            'name' => 'Dutch Ovens',
            'slug' => Str::slug('Dutch Ovens'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 3,
            'name' => 'Saute Pans',
            'slug' => Str::slug('Saute Pans'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 3,
            'name' => 'Overware',
            'slug' => Str::slug('Overware'),
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'parent_category_id' => 3,
            'name' => 'Woks',
            'slug' => Str::slug('Woks'),
            'is_active' => true
        ]);
    }
}

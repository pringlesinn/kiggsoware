<?php

use Illuminate\Database\Seeder;

class suppliers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->insert([
	        'name' => 'Rajan',
	        'company_name' => 'ABC company',
            'street' => '22 loren',
            'city' => 'winnipeg',
            'postal_code' => 'R2D3D2',
            'province' => 'mb',
            'country' => 'canada',
            'email' => 'abc@gmail.com',
            'phone' => '564783975'
	    ]);

	    DB::table('suppliers')->insert([
	        'name' => 'Tom',
	        'company_name' => 'xyz company',
            'street' => 'kildonal place',
            'city' => 'winnipeg',
            'postal_code' => 'a4a7d8',
            'province' => 'mb',
            'country' => 'canada',
            'email' => 'xyz@gmail.com',
            'phone' => '5468751234'
	    ]);

	    DB::table('suppliers')->insert([
	        'name' => 'Jerry',
	        'company_name' => 'jerry Company',
            'street' => 'jonson street',
            'city' => 'winnipeg',
            'postal_code' => 'd5d7g7',
            'province' => 'mb',
            'country' => 'canada',
            'email' => 'jerry@gmail.com',
            'phone' => '7564854678'

	    ]);

	    DB::table('suppliers')->insert([
	        'name' => 'Rahul',
	        'company_name' => 'Rahul Company',
            'street' => 'Henderson Highway',
            'city' => 'winnipeg',
            'postal_code' => 'd5d6d8',
            'province' => 'mb',
            'country' => 'Canada',
            'email' => 'rahul@gmail.com',
            'phone' => '2346785346'
	    ]);

	    DB::table('suppliers')->insert([
	        'name' => 'Tony',
	        'company_name' => 'Tony Comapny',
            'street' => 'Mapel Street',
            'city' => 'winnipeg',
            'postal_code' => 'F4ad7h',
            'province' => 'mb',
            'country' => 'Canada',
            'email' => 'tony@gmail.com',
            'phone' => '34567812345'
	    ]);
    }
}

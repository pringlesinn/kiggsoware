<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class purchaseorders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('purchase_orders')->insert([
            'user_id' => 6,
            'user_name' => 'Gurjot Kaur',
            'user_email' => 'gurjotkaur@gmail.com',
            'shipping_address_json' => '{"address": "324 martin"}',
            'billing_address_json' => '{"address": "324 Henderson"}',
            'sub_total' => 1000,
            'pst' => 2.3,
            'gst' => 1.7,
            'total' => 1003,
            'estimated_delivery' => Carbon::now()->format('Y-m-d H:i:s'),
            'delivery_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_delivered' => true,
            'payment_status' => 'completed'
        ]);

        DB::table('purchase_orders')->insert([
            'user_id' => 6,
            'user_name' => 'Harry Gill',
            'user_email' => 'harry@gmail.com',
            'shipping_address_json' => '{"address": "324 Henderson"}',
            'billing_address_json' => '{"address": "324 Henderson"}',
            'sub_total' => 1000,
            'pst' => 2.3,
            'gst' => 1.7,
            'total' => 1003,
            'estimated_delivery' => Carbon::now()->format('Y-m-d H:i:s'),
            'delivery_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_delivered' => true,
            'payment_status' => 'completed'
        ]);

        DB::table('purchase_orders')->insert([
            'user_id' => 3,
            'user_name' => 'karm',
            'user_email' => 'karm@gmail.com',
            'shipping_address_json' => '{"address": "324 henderson"}',
            'billing_address_json' => '{"address": "324 Henderson"}',
            'sub_total' => 1000,
            'pst' => 2.3,
            'gst' => 1.7,
            'total' => 1003,
            'estimated_delivery' => Carbon::now()->format('Y-m-d H:i:s'),
            'delivery_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_delivered' => false,
            'payment_status' => 'pending'
        ]);

        DB::table('purchase_orders')->insert([
            'user_id' => 5,
            'user_name' => 'gondim',
            'user_email' => 'gomdim@gmail.com',
            'shipping_address_json' => '{"address": "324 downtown"}',
            'billing_address_json' => '{"address": "324 Henderson"}',
            'sub_total' => 1000,
            'pst' => 2.3,
            'gst' => 1.7,
            'total' => 1003,
            'estimated_delivery' => Carbon::now()->format('Y-m-d H:i:s'),
            'delivery_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_delivered' => false,
            'payment_status' => 'pending'
        ]);

        DB::table('purchase_orders')->insert([
            'user_id' => 4,
            'user_name' => 'sidhant',
            'user_email' => 'sidhant@gmail.com',
            'shipping_address_json' => '{"address": "324 mapals"}',
            'billing_address_json' => '{"address": "324 Henderson"}',
            'sub_total' => 1000,
            'pst' => 2.3,
            'gst' => 1.7,
            'total' => 1003,
            'estimated_delivery' => Carbon::now()->format('Y-m-d H:i:s'),
            'delivery_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_delivered' => false,
            'payment_status' => 'pending'
        ]);
    }
}

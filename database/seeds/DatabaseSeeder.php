<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(users::class);
        $this->call(products::class);
        $this->call(suppliers::class);
        $this->call(productpurchaseorder::class);
        $this->call(categories::class);
        $this->call(purchaseorders::class);
        // $this->call(carts::class); KOW-104
        $this->call(useraddresses::class);
        $this->call(transactions::class);
        // $this->call(cartproduct::class); KOW-104
        $this->call(categoryproduct::class);
    }
}

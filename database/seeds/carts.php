<?php

use Illuminate\Database\Seeder;

class carts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('carts')->insert([
	        'user_id' => 1 
	    ]);

	    DB::table('carts')->insert([
	        'user_id' => 2
	    ]);

	    DB::table('carts')->insert([
	        'user_id' => 3
	    ]);

	    DB::table('carts')->insert([
	        'user_id' => 4 
	    ]);

	    DB::table('carts')->insert([
	        'user_id' => 5 
	    ]);

    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class useraddresses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('user_addresses')->insert([
           'user_id' => 1,
           'street' => '123 address street',
           'postal_code' => 'R9g9c9',
           'city' => 'Winnipeg',
           'province' => 'MB',
           'country' => 'Canada'
        ]);

         DB::table('user_addresses')->insert([
          'user_id' => 2,
          'street' => '123 address street',
          'postal_code' => 'R9g9c9',
          'city' => 'Winnipeg',
          'province' => 'MB',
          'country' => 'Canada'
        ]);
        
         DB::table('user_addresses')->insert([
          'user_id' => 3,
          'street' => '123 address street',
          'postal_code' => 'R9g9c9',
          'city' => 'Winnipeg',
          'province' => 'MB',
          'country' => 'Canada'
        ]);

         DB::table('user_addresses')->insert([
          'user_id' => 4,
          'street' => '123 address street',
          'postal_code' => 'R9g9c9',
          'city' => 'Winnipeg',
          'province' => 'MB',
          'country' => 'Canada'
        ]);

         DB::table('user_addresses')->insert([
          'user_id' => 1,
          'street' => '123 address street',
          'postal_code' => 'A0A0A0',
          'city' => 'Toronto',
          'province' => 'MB',
          'country' => 'Canada'
        ]); 
    }
}

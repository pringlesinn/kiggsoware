<?php

use Illuminate\Database\Seeder;

class cartproduct extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('cart_product')->insert([
	        'cart_id' => 1,
	        'product_id' => 1,
	        'quantity' => 10
	    ]);

	    DB::table('cart_product')->insert([
	        'cart_id' => 1,
	        'product_id' => 2,
	        'quantity' => 50
	    ]);

	    DB::table('cart_product')->insert([
	        'cart_id' => 2,
	        'product_id' => 2,
	        'quantity' => 20
	    ]);

	    DB::table('cart_product')->insert([
	        'cart_id' => 3,
	        'product_id' => 3,
	        'quantity' => 30
	    ]);

	    DB::table('cart_product')->insert([
	        'cart_id' => 4,
	        'product_id' => 4,
	        'quantity' => 40
	    ]);

	    DB::table('cart_product')->insert([
	        'cart_id' => 5,
	        'product_id' => 5,
	        'quantity' => 50
	    ]);
    }
}

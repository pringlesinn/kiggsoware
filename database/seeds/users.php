<?php

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Gurjot',
            'last_name' => 'Kaur',
            'email' => 'gurjotkaur@gmail',
            'password' => 'abc',
            'phone' => '4536277345',
            'is_admin' => false
        ]);

        DB::table('users')->insert([
            'first_name' => 'Iqbaljeet',
            'last_name' => 'Sharma',
            'email' => 'iqbal@gmail',
            'password' => 'abc',
            'phone' => '45376547345',
            'is_admin' => true
        ]);

        DB::table('users')->insert([
            'first_name' => 'Karm',
            'last_name' => 'Kaur',
            'email' => 'karmkaur@gmail',
            'password' => 'abc',
            'phone' => '4536232345',
            'is_admin' => false
        ]);
        DB::table('users')->insert([
            'first_name' => 'sidhant',
            'last_name' => 'B',
            'email' => 'sidhant@gmail',
            'password' => 'abc',
            'phone' => '4536237345',
            'is_admin' => false
        ]);

        DB::table('users')->insert([
            'first_name' => 'A',
            'last_name' => 'Gondim',
            'email' => 'gondim@gmail',
            'password' => 'abc',
            'phone' => '4536222345',
            'is_admin' => false
        ]);
    }
}

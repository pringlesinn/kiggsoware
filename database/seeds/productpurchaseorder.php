<?php

use Illuminate\Database\Seeder;

class productpurchaseorder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_purchase_order')->insert([
            'product_id' => 2,
            'purchase_order_id' => 1,
            'unit_price' => 12,
            'quantity' => 10
        ]);

        DB::table('product_purchase_order')->insert([
            'product_id' => 3,
            'purchase_order_id' => 1,
            'unit_price' => 12,
            'quantity' => 15
        ]);

        DB::table('product_purchase_order')->insert([
            'product_id' => 3,
            'purchase_order_id' => 3,
            'unit_price' => 12,
            'quantity' => 17
        ]);

        DB::table('product_purchase_order')->insert([
            'product_id' => 5,
            'purchase_order_id' => 4,
            'unit_price' => 12,
            'quantity' => 19
        ]);

        DB::table('product_purchase_order')->insert([
            'product_id' => 4,
            'purchase_order_id' => 5,
            'unit_price' => 12,
            'quantity' => 22
        ]);
    }
}

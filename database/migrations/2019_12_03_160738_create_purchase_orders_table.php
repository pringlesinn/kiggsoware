<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('user_email');
            $table->text('shipping_address_json');
            $table->text('billing_address_json');
            $table->decimal('sub_total', 9, 2);
            $table->decimal('pst', 6, 2);
            $table->decimal('gst', 6, 2);
            $table->decimal('total', 9, 2);
            //Added raw DB timestamp to work on Maria DB
            $table->timestamp('estimated_delivery')->default(\DB::raw('CURRENT_TIMESTAMP'));
            //Added raw DB timestamp to work on Maria DB
            $table->timestamp('delivery_date')->nullable();
            $table->boolean('is_delivered')->default(false);
            $table->enum('payment_status', ['pending', 'completed'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}

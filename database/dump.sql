-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: kiggs_preprod
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,'Appliances',1,'2019-12-11 23:22:50','2019-12-11 23:22:50','appliances'),(2,1,'Gas Ranges',1,'2019-12-12 02:56:41','2019-12-12 02:56:41','gas-ranges'),(3,1,'Refrigerators',1,'2019-12-12 05:16:40','2019-12-12 05:16:40','refrigerators'),(4,NULL,'Homecoming',1,'2019-12-12 16:44:36','2019-12-12 16:44:36','homecoming'),(5,4,'Wall Hangings',1,'2019-12-12 16:45:36','2019-12-12 16:45:36','wall-hangings'),(6,4,'Air Purifier',1,'2019-12-12 17:58:37','2019-12-12 18:10:30','air-purifier'),(7,NULL,'Food',1,'2019-12-12 18:34:53','2019-12-12 18:34:53','food'),(8,7,'Gifts Basket',1,'2019-12-12 18:35:25','2019-12-12 18:35:25','gifts-basket'),(9,1,'New collection',1,'2019-12-12 19:05:57','2019-12-12 19:05:57','new-collection'),(10,4,'Every day use',1,'2019-12-12 19:06:23','2019-12-12 19:06:23','every-day-use'),(11,1,'Perfect Style',1,'2019-12-12 19:06:37','2019-12-12 19:06:37','perfect-style'),(12,NULL,'Cookware',0,'2019-12-12 19:31:56','2019-12-13 10:16:35','cookware'),(13,12,'Dinnerware Sets',1,'2019-12-12 19:34:59','2019-12-12 19:34:59','dinnerware-sets'),(14,12,'Cutleries Set',1,'2019-12-12 19:52:33','2019-12-12 19:52:33','cutleries-set'),(15,NULL,'Knives',1,'2019-12-12 20:47:10','2019-12-12 20:47:10','knives'),(16,15,'Knife Set',1,'2019-12-12 21:06:51','2019-12-12 21:06:51','knife-set'),(17,15,'Chef\'s Knife',1,'2019-12-12 21:30:22','2019-12-12 21:30:22','chefs-knife'),(18,NULL,'Bar Essentials',1,'2019-12-12 21:48:22','2019-12-12 21:48:22','bar-essentials'),(19,18,'Pitchers',1,'2019-12-12 21:50:46','2019-12-12 21:50:46','pitchers'),(20,18,'Wine Glasses',1,'2019-12-12 22:04:53','2019-12-12 22:04:53','wine-glasses');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_product`
--

DROP TABLE IF EXISTS `category_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_product`
--

LOCK TABLES `category_product` WRITE;
/*!40000 ALTER TABLE `category_product` DISABLE KEYS */;
INSERT INTO `category_product` VALUES (1,2,1,NULL,NULL),(2,2,2,NULL,NULL),(3,2,3,NULL,NULL),(4,2,4,NULL,NULL),(5,2,5,NULL,NULL),(6,3,6,NULL,NULL),(7,3,7,NULL,NULL),(8,3,8,NULL,NULL),(9,3,9,NULL,NULL),(10,3,10,NULL,NULL),(11,5,11,NULL,NULL),(12,5,12,NULL,NULL),(13,5,13,NULL,NULL),(14,5,14,NULL,NULL),(15,5,15,NULL,NULL),(16,5,16,NULL,NULL),(17,5,17,NULL,NULL),(18,6,18,NULL,NULL),(19,6,19,NULL,NULL),(20,6,20,NULL,NULL),(21,8,21,NULL,NULL),(22,8,22,NULL,NULL),(23,8,23,NULL,NULL),(24,8,24,NULL,NULL),(25,8,25,NULL,NULL),(26,13,26,NULL,NULL),(27,13,27,NULL,NULL),(28,13,28,NULL,NULL),(29,13,29,NULL,NULL),(30,13,30,NULL,NULL),(31,14,31,NULL,NULL),(32,14,32,NULL,NULL),(33,14,33,NULL,NULL),(34,14,34,NULL,NULL),(35,14,35,NULL,NULL),(36,16,36,NULL,NULL),(37,16,37,NULL,NULL),(38,16,38,NULL,NULL),(39,17,39,NULL,NULL),(40,17,40,NULL,NULL),(41,19,41,NULL,NULL),(42,19,42,NULL,NULL),(43,19,43,NULL,NULL),(44,20,44,NULL,NULL),(45,20,45,NULL,NULL),(46,9,46,NULL,NULL),(47,11,47,NULL,NULL),(48,20,48,NULL,NULL),(49,19,49,NULL,NULL),(50,20,50,NULL,NULL);
/*!40000 ALTER TABLE `category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2019_12_03_160635_create_products_table',1),(2,'2019_12_03_160724_create_users_table',1),(3,'2019_12_03_160738_create_purchase_orders_table',1),(4,'2019_12_03_160748_create_suppliers_table',1),(5,'2019_12_03_160759_create_categories_table',1),(6,'2019_12_03_160809_create_transactions_table',1),(7,'2019_12_03_160819_create_user_addresses_table',1),(8,'2019_12_03_160834_create_product_purchase_order_table',1),(9,'2019_12_03_160851_create_cart_product_table',1),(10,'2019_12_03_160902_create_category_product_table',1),(11,'2019_12_03_173702_create_removed_datas_table',1),(12,'2019_12_03_184431_create_carts_table',1),(13,'2019_12_07_191433_add_slug_to_categories_table',1),(14,'2019_12_09_014209_add_softdelete_column_to_categories_table',1),(15,'2019_12_09_014854_add_softdelete_column_to_products_table',1),(16,'2019_12_09_015015_add_softdelete_column_to_suppliers_table',1),(17,'2019_12_09_015115_add_softdelete_column_to_users_table',1),(18,'2019_12_09_015203_add_softdelete_column_to_carts_table',1),(19,'2019_12_09_015329_add_softdelete_column_to_user_addresses_table',1),(20,'2019_12_09_033330_delete_removed_datas_table',1),(21,'2019_12_09_071915_delete_softdelet_from_categories_table',1),(22,'2019_12_09_162551_drop_carts_table',1),(23,'2019_12_09_162641_drop_cart_product_table',1),(24,'2019_12_12_073711_add_response_to_transactions_table',2),(25,'2019_12_12_214429_create_password_resets_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('brent@brentscott.com','$2y$10$y/o.BaCCSwqj7QIaPBUNZeaj.MPx0uDZd7nwq9sWa7PJym57xpW.K','2019-12-13 10:18:36');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_purchase_order`
--

DROP TABLE IF EXISTS `product_purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_purchase_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `unit_price` decimal(9,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_purchase_order`
--

LOCK TABLES `product_purchase_order` WRITE;
/*!40000 ALTER TABLE `product_purchase_order` DISABLE KEYS */;
INSERT INTO `product_purchase_order` VALUES (1,1,1,3499.00,1,NULL,NULL),(2,6,2,2500.00,26,NULL,NULL),(3,21,3,450.00,1,NULL,NULL),(4,21,4,450.00,1,NULL,NULL),(5,21,5,450.00,1,NULL,NULL),(6,21,6,450.00,1,NULL,NULL);
/*!40000 ALTER TABLE `product_purchase_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,1,'Front Control Gas Range in Stainless Steel',3499.00,100,'1576125914.jpg','The Professional style gas range is 30\" wide and features a 5.7 cu. Ft. oven capacity, self-cleaning convection oven and 5 burners. 20,000 BTU power burners offer pre-set options for more precise cooking at different temperatures. 4 pane heat resistant cool-touch glass on the oven door to prevent accidental burns',1,'2019-12-12 04:45:14','2019-12-13 09:25:07',NULL),(2,2,'Rear Control Electric Range in Stainless Steel',1795.00,200,'1576126547.jpg','The Frigidaire 30\" Electric Range allows you to get meals on the table faster with our 3,000W element that boils faster than the traditional setting. SpaceWise® Expandable Elements expand to meet your cooking needs - big or small. With the Store-More Storage Drawer, you will have extra space to store your cookware.',1,'2019-12-12 04:55:47','2019-12-13 09:25:28',NULL),(3,3,'Front Control Gas Range in Stainless Steel',7500.00,250,'1576126809.jpg','The Fulgor Milano F6PGR366S1 is a 36\" wide, freestanding professional gas range with 6 18,000 BTU burners and 5.2 cu. Ft. dual convection oven. An integrated dual valve system delivers precise heat on every burner.',1,'2019-12-12 05:00:09','2019-12-13 09:26:18',NULL),(4,1,'Slide-In Electric Range in Fingerprint Resistant Stainless Steel Finish',2000.00,250,'1576127160.jpg','Our electric stove with 5 elements has the power to get all of your favourites sizzling right away. The Power element brings the heat for quick searing, while Power Preheat lets you start cooking sooner. For faster baking and roasting, true convection with a third heating element helps keep temperatures consistent. Plus, you can count on this freestanding stove for a decade of dependability thanks to a 10-year limited parts warranty.',1,'2019-12-12 05:06:00','2019-12-13 09:26:35',NULL),(5,3,'Rear Control Electric Range in Stainless Steel',2800.00,300,'1576127360.jpg','The NE7850 electric range has something of a dual personality. Its 5.9 cu. ft. capacity oven can be used as a single oven or, thanks to Flex Duo technology, can be divided into 2 ovens to cook multiple dishes at different temperatures. Dual true convection cooking also delivers consistent heat no matter how big or small your meals are in size.',1,'2019-12-12 05:09:20','2019-12-13 09:26:54',NULL),(6,3,'Mini Fridge Refrigerator in Black',2500.00,175,'1576127871.jpg','Forget running to the kitchen, enjoy the convenience of chilled beverages where they\'re needed most with our Energy Star® rated Danby Designer® compact all fridge. Boasting a generous capacity of 4.4 cu. ft. this counter high model is an efficient way to supplement your refrigerator. It includes our Canstor® beverage dispenser, reversible door hinge and interior light.',1,'2019-12-12 05:17:51','2019-12-13 09:27:19',NULL),(7,2,'Built In / Integrated Refrigerator in Stainless Steel',8850.00,50,'1576128032.jpg','The Electrolux Icon 32\" All-Refrigerator with 18.6 cu. ft. Capacity, 3 Cantilever Glass Shelves, Gallon Door Storage, Smooth-Glide Crisper Drawers, Custom Set Controls, PureAdvantage Air Filter, Custom-Fit Capacity, Sabbath Mode and Star-K Certified.',1,'2019-12-12 05:20:32','2019-12-13 09:27:45',NULL),(8,3,'French Door Refrigerator in Stainless Steel',8000.00,150,'1576128191.jpg','This Frigidaire four-door French door refrigerator offers the Custom-Flex® Temp Drawer, the only drawer available that fully switches from frig to freezer, so you can store all your family favorites from yogurt to frozen pizza. It helps keep fruits and vegetables fresh longer with CrispSeal® crispers, and it\'s easy to organize and clean with SpaceWise® organization system and Smudge-Proof® stainless steel.',1,'2019-12-12 05:23:11','2019-12-13 09:27:59',NULL),(9,1,'French Door Refrigerator in Stainless Steel',10000.00,90,'1576128332.jpg','This uniquely versatile, platinum interior refrigerator offers 5 doors, including 2 soft-close drawers, to deliver optimized storage. This refrigerator also features the Preserva® Food Care System which uses two independent cooling systems to help keep food fresh longer, while under-shelf LED lighting beautifully displays the food below for greater visibility.',1,'2019-12-12 05:25:32','2019-12-13 09:28:25',NULL),(10,3,'Built In / Integrated Refrigerator in Stainless steel',1250.00,200,'1576128588.jpg','Wine bottle couplings sit row after row on smooth-sliding shelves to keep this wine cellar ultra compact and a reversible door swing adds an extra dose of accessibility. This one won\'t steal much space, but it will steal all the attention.',1,'2019-12-12 05:29:48','2019-12-13 09:28:56',NULL),(11,4,'Multi dimensional blue hanging piece item',500.00,250,'1576170123.jpg','Spice Up Any Space - natural materials, simple and elegant macrame wall decor is perfect for adding that extra \"something\" to your bedroom, living room, dining room, nursery, apartment, office, dorm room, yoga room, sunroom or studio. Makes the perfect statement accent piece for the head of your bed, on the gallery wall , on the hallway by the front door, or small/narrow accent walls, over a couch or by a window for a bohemian touch to any space.',1,'2019-12-12 17:02:03','2019-12-12 17:02:03',NULL),(12,2,'Macrame Woven Wall Hanging Tapestry',400.00,225,'1576171256.jpg','High Quality & Great Size - Made from high quality, cream beige color twisted cotton rope, hung on a 13\" L natural wooden dowel, Macrame size: Approx 29\" L x 11\" W. They will warm your home decoration in the best boho way!',1,'2019-12-12 17:20:56','2019-12-13 09:29:27',NULL),(13,3,'Tapestry Wall Hanging  Night Sky Bohemian Beach Indian',750.00,80,'1576171404.jpg','CLEARLY DESIGN: Made from Microfiber Peach With Clearly Printed Top and Hand-sewn Finished Edges, This Handmade Constellation Tapestry is Infinitely superior to Other Materials. Size measures approx 59.1\" x 51.2\" / 150cm x130cm',1,'2019-12-12 17:23:24','2019-12-13 09:29:54',NULL),(14,1,'Nature Purple Wall Hanging Tapestry Meditation Spa Artwork',750.00,120,'1576171738.jpg','High quality 100% lightweight superior polyester fabric with a printed top. It is comfy, stretchy, skin-friendly and breathing ability.',1,'2019-12-12 17:28:58','2019-12-13 09:30:13',NULL),(15,2,'Calculs Original Boho Gold Shining Moon Phrase Wall Hanging',550.00,150,'1576172035.jpg','The design of the moon phase, add a delicate sense of mystery, beautiful and elegant metal decoration, you can adjust the spacing position yourself, hang in the wedding, party at will. Color: gold.',1,'2019-12-12 17:33:55','2019-12-13 09:30:32',NULL),(16,4,'Essential Oil Diffusers, Aroma Diffuser',450.00,275,'1576173682.jpg','This diffuser adopts ultrasonic technology that provides whisper smooth mist and pleasant aromas to create a calm, relaxing environment, soften and moisten dry and chapped skin and helps you breathe better in all weather.',1,'2019-12-12 18:01:22','2019-12-13 09:31:22',NULL),(17,3,'ASAKUKI 300ML Essential Oil Diffuser, Wood Grain BPA-Free Whisper Quiet 5-In-1 Cool Mist Humidifier, Natural Home Fragrance Diffuser with 14 Colors LED Lights and Easy to Clean - Light Brown',150.00,130,'1576174040.jpg','This ultrasonic essential oil diffuser is an amazing multifunction aromatherapy device unlike any other you\'ve ever used. It features a large and easy to clean 300ml water tank, 7 different LED light colors, multiple mist nebulizer modes, as well as a safety auto-switch that prevents it from overheating in case it runs out of water.',1,'2019-12-12 18:07:20','2019-12-12 18:07:20',NULL),(18,1,'Ultrasonic Aroma Diffuser - Metal Forest Pattern 500ml Essential Oil Diffuser Aromatherapy Diffusers for Essential Oils',800.00,200,'1576174257.jpg','500ml large capacity oil defusers humidifier: Fine mists to prevent static electricity, soothe itchy skin, and more comfortable breathing in the dry winter air.',1,'2019-12-12 18:10:57','2019-12-12 18:10:57',NULL),(19,3,'Aromatherapy Essential Oil Diffuser 100 ml, Ultrasonic Cool Mist Aroma Oil Diffuser, Whisper Quiet Humidifier with Time Setting and 7 Colors LED Lights Changing for Home Office (3D)',150.00,200,'1576174402.jpg','*Hand Made Glass Diffuser & More Elegant Design* QUSUP Aromatherapy Essential Oil Diffuser is Glass Marble Pattern vase design, a perfect addition to any contemporary home, office, gym, spa or studio. Compact and portable, the aroma diffuser’s cover can be effortlessly removed, easy to fill with water and a few drops of essential oil, helping you readily reap aromatherapy benefits.',1,'2019-12-12 18:13:23','2019-12-12 18:13:23',NULL),(20,1,'ETAROWKS New Version Aromatherapy Essential Oil Diffuser Humidifier Ultrasonic Color Light Changing Waterless Auto Off for Baby Home Office Yoga Birthday Gift Decorative',900.00,45,'1576175209.jpg','3D essential oil diffuser is elegant in design and charming in color. The 3D effect is made of glass and a unique process. It\'s even more stunning with a silver base. We use high-quality environmental protection polypropylene products, without BPA. The diffuser has a 100ml water tank. When the water is run out, the diffuser will automatically shut off and protect itself.',1,'2019-12-12 18:26:49','2019-12-12 18:26:49',NULL),(21,1,'ClearBags 14 x 24 Large Holiday Gift Basket Bags | Perfect for Christmas Parties Gift and Food Baskets',450.00,100,'1576175988.jpg','seal your baked goods, candies and gifts while also displaying them with a protective crystal clear plastic. Designed specifically for wrapping gift baskets, these bags feature a unique large round bottom that will ensure even coverage and easy wrapping. Our cello crinkle Gift Basket Bags twist to seal your contents and create a cute and functional handle',1,'2019-12-12 18:39:48','2019-12-12 18:39:48',NULL),(22,3,'Forart Christmas Candy Storage Basket Christmas Decoration Cute Santa Deer Snowman Candy Storage Basket Storage Christmas Desktop Decor Xmas Gift',780.00,100,'1576177029.jpg','Beautiful decoration: Cute dolls add a sense of cuteness to your Christmas, bamboo baskets can also help you store candy. This is a lovely and practical Christmas decoration. Multi-purpose: can be used to hold candy, biscuits, any other small gifts.',1,'2019-12-12 18:57:09','2019-12-12 18:57:09',NULL),(23,3,'All About the Baskets Gourmet Chocolate Gift Basket Filled with Quality Chocolate Delights',800.00,200,'1576177272.jpg','CHOCOLATE FOR A CHOCOHOLIC. Find inside a variety of Chocolates including both dark & milk chocolate\r\nA SMALL GIFT BASKET WITH A LOT IN IT INCLUDING ICE WINE CHOCOLATES, A LINDT DARK CHOCOLATE BAR & more!!!',1,'2019-12-12 19:01:12','2019-12-12 19:01:12',NULL),(24,1,'Delicious Holiday Gift Basket of Gourmet Chocolates and Sweet Goodies',500.00,200,'1576177438.jpg','A GREAT GIFT BASKET FOR ANYONE that comes with Toblerone, Lindt, Ghirardelli and more. A SMALL BASKET FILLED WITH GOURMET TREATS perfect to send to family or friends.\r\nINCLUDES TOBLERONE, SOFT COOKIES, GHIRARDELLI CHOCOLATE & more',1,'2019-12-12 19:03:58','2019-12-12 19:03:58',NULL),(25,2,'Christmas, Holiday Love Chocolate Gift Basket - Champagne Bottle Filled with Truffles, Fudge, Cookies, Pirouline, Nuts',1450.00,100,'1576177582.jpg','The Ultimate Chocolate Gift Basket. Create Delightful, Lasting & Surprising Memories by Sending This\r\nPerfect Gift for Holidays, Christmas, Valentines, Corporate, Family, Friends & Loved Ones',1,'2019-12-12 19:06:22','2019-12-12 19:06:22',NULL),(26,3,'Elite Bella Galleria 16-Piece Dinnerware Set Service for 4',1200.00,50,'1576179477.jpg','Experience the luxury and craftsmanship of high-quality dishware with the Gibson Elite Bella Galleria Dinnerware set. This 16-Piece set is crafted from Gibson’s high-quality stoneware.\r\n\r\nThe design of this set is traditional, with a modern twist. The stoneware is covered by Gibson’s reactive glaze; an artistic finish that produces unique results, ensuring no two pieces are exactly alike.',1,'2019-12-12 19:37:57','2019-12-12 19:37:57',NULL),(27,1,'Gibson Couture Bands 16-Piece Dinnerware Set, Blue and Cream',300.00,200,'1576179570.jpg','Gibson Elite Couture Bands 16-Piece Dinnerware Set, Blue and Cream - With Elite, Gibson\'s housewares manufacturing expertise takes material and technique to the next level.  Elite collections are perfect for those who appreciate tabletop style statements in their own homes or as beautiful giftware.  Found in the world\'s finest retailers, Gibson Elite dinnerware, flatware and more represents elegance and fashion leadership that helps to make dinner with friends and family truly memorable occasions. .',1,'2019-12-12 19:39:30','2019-12-12 19:39:30',NULL),(28,3,'Elite Clementine 12 Piece Reactive Glaze Dinnerware Set Service for 4, Blue',900.00,250,'1576179827.jpg','Your Gibson Elite Clementine dinnerware features a beautiful reactive glaze framed with a soft white trim. For an added touch the glaze has speckled accents randomly expressed like a dash of paint-spattered on a palette. Bold and beautiful your dinnerware will earn the phrase \"Oh my darling Clementine\". Service for 4 to adorn your table.',1,'2019-12-12 19:43:47','2019-12-12 19:43:47',NULL),(29,4,'Pfaltzgraff Trellis White 16-Piece Stoneware Dinnerware Set, Service for 4',200.00,100,'1576180053.jpg','This 16-piece dinnerware set, service for four, includes 4 each of 11-inch dinner plate, 8-inch salad plate, 6-inch soup cereal bowl, and 14-ounce mug. Made from high-quality stoneware for long-lasting beauty and durability.',1,'2019-12-12 19:47:33','2019-12-12 19:47:33',NULL),(30,2,'Gibson Milanto 16-Piece Dinnerware Set Featuring Embossed Reactive Glaze Stoneware Plates, Bowls & Mugs Grey',800.00,70,'1576180278.jpg','The Milanto collection is a beautiful artisinal embossed reactive glaze dinnerware inspired by classic Italian art and design. Your guests will be captivated by the magnificent presentation of embossed reactive glaze on stoneware. The rich grey color will compliment any meal and create memorable tablescapes for your dining pleasure.',1,'2019-12-12 19:51:18','2019-12-12 19:51:18',NULL),(31,3,'Berglander Flatware Set Black Gold, 20 Piece Black Flatware, 20 Piece Black Titanium Flatware, 20 Piece Black Gold Plated Stainless Steel Silverware Set Cutlery Sets, Service for 4',600.00,100,'1576180873.jpg','4 each of dinner fork, Salad Fork dinner knife, tablespoon, and teaspoon. Service for 4. Adopting high-quality food safety material, poisonless and tasteless.',1,'2019-12-12 20:01:13','2019-12-12 20:01:13',NULL),(32,3,'Lillian Tablesettings 37283 Plastic Cutlery Silverware Extra Heavyweight Disposable Flatware, Full Size Cutlery Combo, Polished Gold, 80 Forks, Value Pack 160 Count',400.00,100,'1576181236.jpg','Plastic Cutlery Silverware Extra Heavyweight Disposable Flatware, Full Size Cutlery Combo, Polished Gold, 80 Forks, 40 Spoons, 40 Knives, Value Pack 160 Count.',1,'2019-12-12 20:07:16','2019-12-12 20:07:16',NULL),(33,4,'Flatware Set, Magicpro Modern Royal 24-Pieces gold Stainless Steel Flatware for Wedding Festival Christmas Party, Service For 6',900.00,100,'1576182159.jpg','Kiggs \'O\' Ware is committed to providing each with the highest standard of .and good quality products. The streamlined form in the special color of gold exquisite carved enhances the nice appearance. The massive face and smooth edge of spoon protect your lips and tongue from injury; the fit with the center of palm and finger bend improves your dining happiness! A nice set of gorgeous flatware is definitely more fascinating than delicacies. It is suitable for family gathering, party, camper, restaurant, hotel, wedding and more. Also, it’s a great gift for your friends and families.',1,'2019-12-12 20:22:39','2019-12-12 20:22:39',NULL),(34,4,'Buyer Star 4 Set Stainless Steel Chopsticks Spoons Set 4 Color Metal Reusable Korean Chopstix and Spoons Dinnerware Set',800.00,200,'1576182507.jpg','Made of high grade 304 (18/8) stainless steel , heat-resistant and rust-resistant.\r\nHigh hardness, durable, affordable and long-lasting.\r\nThe color of the chopsticks spoons set is electroplated and the painting would not peel off. \r\nStainless steel Set is smooth and exquisite, feels good, looks shiny and beautiful, enhance everyday dining to a sophisticated dining experience.',1,'2019-12-12 20:28:27','2019-12-12 20:28:27',NULL),(35,1,'Brightbuy 5 Set Chopsticks Spoon, Reusable Stainless Steel Chopstix Spoon Set Metal Utensils Silverware Flatware Tableware',800.00,100,'1576183196.jpg','Our chopsticks and dinner spoon are made of premium quality stainless steel which could be easily cleaned and have a long service life. With the lowest investment, you can enjoy your food with the best hygiene safety.',1,'2019-12-12 20:39:56','2019-12-12 20:39:56',NULL),(36,3,'Zwilling Kanren 7-Piece Knife Set With Acacia Block',200.00,200,'1576185094.jpg','Named after the Japanese word for \"connection,\" Zwilling\'s Kanren collection unites German engineering with Japanese craftsmanship. Hand-honed Honbazuke blades slice, dice, chop and carve with scalpel-like sharpness thanks to high-performance carbide stainless-steel. Triple-riveted Micarta handles feature Zwilling\'s signature curved bolster, promoting a secure grip and comfortable, fatigue-free cutting.',1,'2019-12-12 21:11:34','2019-12-12 21:11:34',NULL),(37,3,'Zwilling Stainless-Steel 8-Piece Knife Set With Tray',900.00,200,'1576185883.jpg','Designed in Italy and expertly engineered in Germany, these professional-grade steak knives are made of sleek stainless steel from base to tip. Serrated blades slice smoothly through the thickest cuts of meat and hold their sharpness over time. The set comes in a beechwood box that\'s ideal for gift-giving and storage.',1,'2019-12-12 21:24:43','2019-12-12 21:24:43',NULL),(38,3,'Shun Premier 12-Piece Knife Set',200.00,250,'1576186181.jpg','The Shun Premier collection is distinguished by hand-hammered blades and walnut PakkaWood handles that evoke the artistry of traditional hand-forged knives. Handcrafted in Japan, the blades are thin and lightweight. This 12-piece set goes beyond the basics with a full complement of cutlery for the home cook, including specialized Japanese knives, a honing steel and sleek wooden storage block',1,'2019-12-12 21:29:41','2019-12-12 21:29:41',NULL),(39,1,'Shun Fuji Chef\'s Knife',500.00,200,'1576186577.jpg','When it comes to fast, precise chopping and slicing, this is the ultimate all-purpose knife for cooks who favour the lightness and super-sharpness of Eastern cutlery. The slightly oversize, deep-bellied blade features a core of SG2 Japanese steel that holds an exquisitely fine edge with unparalleled durability. Inspired by the high-performance knives used by Japanese chefs, our exclusive Shun Fuji collection represents the very best in cutlery craftsmanship and technology.',1,'2019-12-12 21:36:17','2019-12-12 21:36:17',NULL),(40,3,'Wüsthof Classic Ikon Chef\'s Knives',400.00,100,'1576186769.jpg','Perfectly balanced to feel like an extension of your hand, this chef\'s knife deftly slices, dices, minces and juliennes. Its high-carbon-steel blade, forged in the renowned cutlery centre of Solingen, Germany, tapers from handle to tip and from spine to edge.',1,'2019-12-12 21:39:29','2019-12-12 21:39:29',NULL),(41,4,'AERIN White Confetti Pitcher',500.00,250,'1576187713.jpg','One-of-a-kind character is a hallmark of handcrafted glassware like our Confetti collection. This pitcher is mouth-blown from clear glass embedded with opaque white glass – the organic formations make each piece unique. Designed in collabouration with Aerin Lauder, the pitcher reflects her philosophy that living beautifully should be effortless.',1,'2019-12-12 21:55:13','2019-12-12 21:55:13',NULL),(42,3,'Paz Pitcher',900.00,500,'1576188022.jpg','Mixed materials make our pitcher an artisanal complement to handcrafted cocktails. Hand cut with a graphic grid, the clear glass pitcher fits into a bronze-finished stainless-steel frame with a supple leather handle. A well-shaped spout ensures smooth pouring and serving.',1,'2019-12-12 22:00:22','2019-12-12 22:00:22',NULL),(43,1,'RBT Wine Decanter',650.00,100,'1576188164.jpg','Designed with true wine lovers in mind, this sleekly styled decanter effectively filters and aerates wines, while presenting beautifully for formal occasions. The traditionally shaped glass vessel is fitted with a gold-toned, stainless-steel aerating funnel that oxygenates wine by flowing liquid across the inside walls of the glass. A micro-perforated strainer removes impurities and sediment, further enhancing the flavor and body of wines. A coordinating gold-finished wood coaster catches drips.',1,'2019-12-12 22:02:44','2019-12-12 22:02:44',NULL),(44,3,'Riedel Vinum XL Cabernet Wine Glasses',550.00,500,'1576188484.jpg','Experts with the world\'s most discerning palates helped develop Riedel\'s Vinum series, a glassware collection designed to optimize specific types of wine and liquor. These Cabernet glasses feature generously sized bowls to enhance the aroma and flavour of a full-bodied red wine.',1,'2019-12-12 22:08:04','2019-12-12 22:08:04',NULL),(45,3,'Williams Sonoma Open Kitchen Stemless Red Wine Glasses',450.00,500,'1576188635.jpg','We\'ve designed our stemless wine glasses to be utilitarian yet beautiful, so they\'re great for both casual entertaining and everyday use. This well-priced European glassware is made of sparkling lead-free crystal for a timeless look with exceptional durability. The size and shape of the bowls make these versatile tumblers perfect for serving all your favorite red wines.',1,'2019-12-12 22:10:35','2019-12-12 22:10:35',NULL),(46,3,'Thor Kitchen Free Standing&Slide- in Gas Range 6 Burners 5.2cu.ft Oven',4500.00,80,'1576189077.jpg','1.1.professional premium-grade stainless steel with chamfered,hand- finished edges.\r\n2.Built-in& Free Standing&Enclosed installation\r\n3.Automatic re-ignition if the burner flame extinguishes\r\n4.7.infrared broil burner high-end broiler radiates the heat to sear your food and seat in the juices.\r\n5.Two Years Warranty',1,'2019-12-12 22:17:57','2019-12-12 22:17:57',NULL),(47,3,'Marshall MF3.2-NA Medium Capacity Bar Fridge, Black',5850.00,450,'1576189340.jpg','Arctic Blue Interior LED Light\r\nAdjustable Front Leg\r\nAdjustable Tempered Glass Shelves\r\nCFC-free',1,'2019-12-12 22:22:20','2019-12-12 22:22:20',NULL),(48,3,'Nurses Don’t Cry We Wine',500.00,200,'1576267339.jpg','Enjoy a relaxing evening with 15 ounces of your favourite white for red wine in our high quality stemless wine glass. The glass is made of extra thick crystal clear glass to protect it from chipping or breaking and is printed with superior ceramic inks.',1,'2019-12-13 13:59:11','2019-12-13 14:02:19',NULL),(49,2,'San Jamar Perfect Pour Shatter-Resistant Beer Pitcher',800.00,150,'1576267727.jpg','Creates Perfect Head — the angled bottom is designed to create the ideal angle to reduce beer turbulence and create the optimum head\r\nLow Profile Design — a wide base and lower height help prevent accidental spillage during transport.',1,'2019-12-13 14:08:47','2019-12-13 14:08:47',NULL),(50,4,'Crystal Wine Glasses - Tall Handcrafted Red or White Wine Glass',400.00,100,'1576267953.jpg','These wine glasses, set of 2 are perfect for you to enjoy full-bodied red wine, with a tapered rim that directs the flow of the wine toward the front palate and highlights the rich fruit while tempering the acidity.',1,'2019-12-13 14:12:33','2019-12-13 14:12:33',NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_orders`
--

DROP TABLE IF EXISTS `purchase_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address_json` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address_json` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(9,2) NOT NULL,
  `pst` decimal(6,2) NOT NULL,
  `gst` decimal(6,2) NOT NULL,
  `total` decimal(9,2) NOT NULL,
  `estimated_delivery` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_delivered` tinyint(1) NOT NULL DEFAULT '0',
  `payment_status` enum('pending','completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_orders`
--

LOCK TABLES `purchase_orders` WRITE;
/*!40000 ALTER TABLE `purchase_orders` DISABLE KEYS */;
INSERT INTO `purchase_orders` VALUES (1,1,'Sidhant Bindra','sidhantbindra@gmail.com','{\"shipping_street\":\"853 Sheppard street\",\"shipping_city\":\"winnipeg\",\"shipping_postal_code\":\"R2P0L2\",\"shipping_province\":\"Manitoba\",\"address\":\"853 Sheppard street, winnipeg\\/Manitoba, R2P0L2\"}','{\"billing_street\":\"853 Sheppard street\",\"billing_city\":\"winnipeg\",\"billing_postal_code\":\"R2P0L2\",\"billing_province\":\"Manitoba\",\"address\":\"853 Sheppard street, winnipeg\\/Manitoba, R2P0L2\"}',3499.00,174.95,244.93,3918.88,'2019-12-12 20:52:51','2019-12-12 20:52:51',0,'pending','2019-12-12 20:52:51','2019-12-12 20:52:51'),(2,3,'Adriano Gondim','pringlesinn@gmail.com','{\"shipping_street\":\"7 Havelock Ave\",\"shipping_city\":\"Winnipeg\",\"shipping_postal_code\":\"R3C3X3\",\"shipping_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}','{\"billing_street\":\"7 Havelock Ave\",\"billing_city\":\"Winnipeg\",\"billing_postal_code\":\"R3C3X3\",\"billing_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}',65000.00,3250.00,4550.00,72800.00,'2019-12-20 10:13:04','2019-12-13 16:13:04',0,'pending','2019-12-13 10:13:04','2019-12-13 10:13:04'),(3,3,'Adriano Gondim','pringlesinn@gmail.com','{\"shipping_street\":\"7 Havelock Ave\",\"shipping_city\":\"Winnipeg\",\"shipping_postal_code\":\"R3C3X3\",\"shipping_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}','{\"billing_street\":\"7 Havelock Ave\",\"billing_city\":\"Winnipeg\",\"billing_postal_code\":\"R3C3X3\",\"billing_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}',450.00,22.50,31.50,504.00,'2019-12-20 11:59:39','2019-12-13 17:59:39',0,'pending','2019-12-13 11:59:39','2019-12-13 11:59:39'),(4,3,'Adriano Gondim','pringlesinn@gmail.com','{\"shipping_street\":\"7 Havelock Ave\",\"shipping_city\":\"Winnipeg\",\"shipping_postal_code\":\"R3C3X3\",\"shipping_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}','{\"billing_street\":\"7 Havelock Ave\",\"billing_city\":\"Winnipeg\",\"billing_postal_code\":\"R3C3X3\",\"billing_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}',450.00,22.50,31.50,504.00,'2019-12-20 13:33:15','2019-12-13 19:33:15',0,'pending','2019-12-13 13:33:15','2019-12-13 13:33:15'),(5,3,'Adriano Gondim','pringlesinn@gmail.com','{\"shipping_street\":\"7 Havelock Ave\",\"shipping_city\":\"Winnipeg\",\"shipping_postal_code\":\"R3C3X3\",\"shipping_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}','{\"billing_street\":\"7 Havelock Ave\",\"billing_city\":\"Winnipeg\",\"billing_postal_code\":\"R3C3X3\",\"billing_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}',450.00,22.50,31.50,504.00,'2019-12-20 13:33:39','2019-12-13 19:33:39',0,'pending','2019-12-13 13:33:39','2019-12-13 13:33:39'),(6,3,'Adriano Gondim','pringlesinn@gmail.com','{\"shipping_street\":\"7 Havelock Ave\",\"shipping_city\":\"Winnipeg\",\"shipping_postal_code\":\"R3C3X3\",\"shipping_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}','{\"billing_street\":\"7 Havelock Ave\",\"billing_city\":\"Winnipeg\",\"billing_postal_code\":\"R3C3X3\",\"billing_province\":\"MB\",\"address\":\"7 Havelock Ave, Winnipeg\\/MB, R3C3X3\"}',450.00,22.50,31.50,504.00,'2019-12-20 13:35:54','2019-12-13 19:35:54',0,'completed','2019-12-13 13:35:54','2019-12-13 13:35:54');
/*!40000 ALTER TABLE `purchase_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` char(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Zyko','ZNS Services Ltd','1178 Turkish Avenue','London','V3K6H1','Ontario','Canada','zyko@services.com','9584561122','2019-12-12 03:04:42','2019-12-12 03:04:42',NULL),(2,'Tony Singh','FLATIRONS RESOURCES LLC','11080 CIRCLE POINT RD','Vancouver','R3E5R4','British Columbia','Canada','tony@flaitrons.com','303292-3902','2019-12-12 03:10:11','2019-12-12 03:10:11',NULL),(3,'Pablo Escobar','ROXANNA NEBRASKA, LLC','952 ECHO LN','Winnipeg','V4Z2T5','Manitoba','Canada','pablo@roxanno.com','7135201153','2019-12-12 03:12:34','2019-12-12 03:12:34',NULL),(4,'Evgheni Carlos','H2 RESOURCES LLC','19436 235th ST','Brandon','E2Z3T4','Manitoba','Canada','ecarlos@h2resources.com','4825662312','2019-12-12 15:52:16','2019-12-12 15:52:16',NULL);
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `reference_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_json` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,6,'K1662373077357046168W','{\"ref_number\":\"K1662373077357046168W\",\"result_code\":\"ok\",\"result_message\":\"Connection successful\",\"transaction_response\":{\"response_code\":\"1\",\"auth_code\":\"2019-14322\",\"errors\":[],\"trans_id\":14322}}','2019-12-13 13:35:54','2019-12-13 13:35:54');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_addresses`
--

DROP TABLE IF EXISTS `user_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` char(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_addresses`
--

LOCK TABLES `user_addresses` WRITE;
/*!40000 ALTER TABLE `user_addresses` DISABLE KEYS */;
INSERT INTO `user_addresses` VALUES (1,1,'853 Sheppard street','winnipeg','R2P0L2','Manitoba','Canada','2019-12-12 20:52:51','2019-12-12 20:52:51',NULL),(2,3,'7 Havelock Ave','Winnipeg','R3C3X3','MB','Canada','2019-12-13 10:13:04','2019-12-13 10:13:04',NULL);
/*!40000 ALTER TABLE `user_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Sidhant','Bindra','sidhantbindra@gmail.com','$2y$10$bMtNbtzx2Cv59d.Z5fQu2.8y69xHZ73mxg3qLC19LHGyYQAvOTPxa','4313883969',1,'2019-12-11 15:00:14','2019-12-11 15:00:14',NULL),(2,'Adriano','Gondim','pringlesinn@gmail.com','$2y$10$AiLQwxTdvyVkdYzG4heRAOH0owgQeTAwlUW1aAHYkOVwM51BcS2mS','4315887086',0,'2019-12-12 04:47:34','2019-12-13 09:34:15','2019-12-13 09:34:15'),(3,'Adriano','Gondim','pringlesinn@gmail.com','$2y$10$gOictVLBmFzhqqel52tVauFh6tVJiJcalNgdAfq2g4vQ8QVJWMyf.','4315887086',0,'2019-12-13 09:30:36','2019-12-13 09:30:36',NULL),(4,'Brent','Scott','brent@brentscott.com','$2y$10$27TKJUKuXjp.b3vAxwqCgOQtOTVIwe8qEmlG/SIcb7zXgNO4xrtia','2042544877',0,'2019-12-13 10:18:14','2019-12-13 10:18:14',NULL),(5,'Steve','George','edu@pagerange.com','$2y$10$PqkgQNS3hNwt2.phe7ggkOhHUuFQxLlYOv7D/BVHd13eujReytVei','4311231234',0,'2019-12-13 13:53:29','2019-12-13 13:53:29',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-13 20:38:16

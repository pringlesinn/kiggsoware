@extends('layouts.kiggs')

@section('content')

    <!-- Main Banner -->
    <div class="banner">
        <img src="images/banner.jpg" alt="Hero image">
    </div>
    <!-- //Main Banner -->
    <!-- Main Content -->
        <!-- Row Content -->
        <div class="rowOne">
            <h2>New Collection</h2>
            <div class="clearFix"></div>
            <div class="calloutLeft">

                <?php 
                    $id = $new_collection->parent_category_id;
                    $subcategoryslug= $new_collection->slug;
                    $parent=$new_collection->findSlug($id);
                    foreach($parent as  $value): 
                ?>
                <a href="/products/category/{{$value->slug}}/{{$subcategoryslug}}">
                    <span>New Collection</span>
                    <img src="images/row-1-callout-1.jpg" alt=" New Collection" height="207">
                </a>
            <?php endforeach; ?>
            </div>
            <div class="calloutCenter">
                <?php 
                    $id = $every->parent_category_id;
                    $subcategoryslug= $every->slug;
                    $parent=$new_collection->findSlug($id);
                    foreach($parent as  $value): 
                ?>
                <a href="/products/category/{{$value->slug}}/{{$subcategoryslug}}">
                    <span>Every Day Use</span>
                    <img src="images/row-1-callout-2.jpg" alt="Every Day Use">
                </a>
                <?php endforeach; ?>
            </div>
            <div class="calloutRight">
                <?php 
                    $id = $perfect->parent_category_id;
                    $subcategoryslug= $perfect->slug;
                    $parent=$new_collection->findSlug($id);
                    foreach($parent as  $value): 
                ?>
                <a href="/products/category/{{$value->slug}}/{{$subcategoryslug}}">
                    <span>Perfect Style</span>
                    <img src="images/row-1-callout-3.jpg" alt="Perfect Style" height="207">
                </a>
            <?php endforeach; ?>
            </div>
        </div>
        <!-- //Row Content -->
        <div class="clearFix"></div>
        <!-- Row Content -->
        <div class="rowTwo">
            <h2>Popular Products</h2>
           
            @foreach($products as $product)

            <div class="callout" class="indexBack">
                <div class="product_background">
                    <?php $categories = $product->categories;

                    foreach($categories as $cat): 
                        $cat_slug = $cat->slug; 
                        $id= $cat->parent_category_id;
                        $parent=$cat->findslug($id);
                        foreach($parent as $pro):
                         ?> 
                            <a href="/products/category/{{$pro->slug}}/{{$cat_slug}}/{{$product->id}}">
                       <?php
                        endforeach;
                     endforeach;
                     ?> 
                        <img src="/images/uploaded/{{$product->image}}" height="250" width="250"  alt="Product image">
                        <div class="collout-span">
                        <span>
                            <p class="namePro truncate">{{ucwords($product->name)}}</p>
                            <p>${{$product->price}}</p>
                            <p class="truncate">{{$product->description}}</p>
                        </span>
                    </div>
                    </a>
                </div>            
            </div>
            @endforeach
        
        <!-- //Row Content -->
@endsection
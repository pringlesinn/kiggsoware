@extends('layouts.kiggs')

@section('content')
		<!-- Main Banner -->
		<div class="banner">
			<img src="images/hero.jpg" alt="">
		</div>
		<div class="aboutPage">
			<h1>About us</h1>
			<div class="aboutLeft">
				<img src="images/top-image.jpg" alt="Our Aim">
				<div class="content">
				<h2>Why Choose us?</h2>
				<p>Kiggs 'O' Ware, Inc. is a multi-channel specialty retailer of high quality products for the kitchen.

In 1980, our founder, Chuck Kiggs, turned a passion for cooking and eating with friends into a small business with a big idea. He opened a store in Ware, Manitoba, to sell the French-Canadian cookware that intrigued him while visiting Europe but that could not be found in Canada. Chuck's business, which set a standard for customer service, took off and helped fuel a revolution in North American cooking and entertaining that continues today.</p>
				</div>
			</div>

			<div class="clearFix"></div>
			<div class="aboutRight">
				<img src="images/bottom-image.jpg" alt="Why Choose Us?">
				<div class="content">
					<h2>Our Aim</h2>
					<p>Today, Kiggs 'O' Ware, Inc. is one of the Canada's largest e-commerce retailers with some of the best known and most beloved brands in kitchen furnishings. We currently operate retail stores in Canada. Our goal is to expand worldwide.</p>
			</div>
		</div>
	</div>
@endsection
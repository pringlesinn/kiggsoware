<!-- Main Navigation -->
<div class="mainNav slideup" id="navigation">
    <ul>
    	<?php 
    		$currentCategory = '';
    		if (strpos(url()->current(), '/products/category/') != -1) {
    			$urlPieces = explode('/products/category/', url()->current());
    			if (count($urlPieces) > 1) {
    				$categorySubcategory = explode('/', $urlPieces[1]);
    				$currentCategory = $categorySubcategory[0];
    			}
    		}
    	 ?>

        @foreach($navigationOptions as $navigationItem)
            <li><a class="{{$currentCategory == $navigationItem->slug ? 'current' : ''}}" href="/products/category/{{$navigationItem->slug}}">{{ucwords($navigationItem->name)}}</a></li>
        @endforeach
    </ul>
</div>
<!-- //Main Navigation -->
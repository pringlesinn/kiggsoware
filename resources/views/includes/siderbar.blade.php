 <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/admin/users">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Category</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Manage Categories</h6>
          <a class="dropdown-item" href="/admin/categories">Parent Category</a>
          <a class="dropdown-item" href="/admin/sub_categories">Sub Category</a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/products">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Products</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/purchase_orders">
          <i class="fas fa-fw fa-table"></i>
          <span>Orders</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/suppliers">
          <i class="fas fa-fw fa-table"></i>
          <span>Suppliers</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="/admin/transactions">
          <i class="fas fa-fw fa-table"></i>
          <span>Transactions</span></a>
      </li>
    </ul>
@extends('layouts.kiggs')
@section('content')
		<!-- Main Content -->
		<div class="col-md">
			<div class="mainContent background-yellow">
				<h1 class="color">Order Details</h1>
				<hr>
				<?php foreach ($purchase_orders as $purchase_order): ?>
				<div class="colOne-order">
					<div class="callout-border">
						<div class="callout1-add-order">
							<h3>Billing Address</h3>
							<i>Address</i>
							<address>
							<?php $billing_address = json_decode($purchase_order->billing_address_json); ?>
							{{$billing_address->address}}
							</address>
						</div>
						<hr>
						<div class="callout1-info-order">
								<h3>Shipping Address</h3>
								<i>Address</i>
								<address>
								<?php $shipping_address = json_decode($purchase_order->shipping_address_json); ?>
								{{$shipping_address->address}}
								</address>
						</div>
						<hr>
						<div class="clearFix"></div>

						<div class="callout2-add-order">
							<div class="callout-table">
								<h2>Products</h2>
								<table id="order-details">
									<tr class="table-border">
										<th style="width: 70%">Name</th>
										<th>Quantity</th>
										<th>Price</th>
									</tr>
									<?php 

									foreach($data as $product):
									 ?>
									<tr class="table-border">
										<td>{{$product->name}}</td>
										<td>{{$product->quantity}}</td>
										<td>{{$product->unit_price}}</td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
						</div>
						<div class="clearFix"></div>
					</div>
				</div>
				<div class="colTwo-order">
					<div class="callout-border-order">
						<div class="callout-table">
							<h2>Placed Order</h2>
							<hr>
							<table class="table-border">
								<tr>
									<th>SubTotal</th>
									<td>{{$purchase_order->sub_total}}</td>
								</tr>
								<tr>
									<th>GST</th>
									<td>{{$purchase_order->gst}}</td>
								</tr>
								<tr>
									<th>PST</th>
									<td>{{$purchase_order->pst}}</td>
								</tr>
								<tr>
									<th>Total</th>
									<td>{{$purchase_order->total}}</td>
								</tr>
								<tr>
									<th>Payment Status</th>
									<td>{{ucwords($purchase_order->payment_status)}}</td>
								</tr>
								<tr>
									<th>Arriving on</th>
									<td>{{date('d M, Y',  strtotime($purchase_order->estimated_delivery))}}</td>
								</tr>
								
							</table>
						</div>
					</div>
				</div>
			<?php endforeach;?>
				<div class="margin-btw-callout"></div>
				<div class="clearFix"></div>
				<!-- //Row Content -->
			</div>
		</div>
@endsection
@extends('layouts.kiggs')

@section('content')
		
		<!-- Main Banner -->
		<div class="banner">
			<img src="/images/hero.jpg" alt="">
		</div>
		<div class="checkoutPage">
			<div class="checkoutContainer">

				<!--- Billing Details Left side start here -->

				<!--- Billing Details (Left side) start here -->

				<div id="billing_address" class="billingDetails">
					<form id="order_placement" class="bd_shipping_form" action="/checkout" method="POST">
						@csrf
						<h2>Billing Details</h2>
						<div class="bd_address">
							@include('includes.error')
							<div class="bd_left">
								@if(!is_null($billingAddress->street))
									<input type="hidden" id="has_previous_billing" name="has_previous_billing" value="true">
									<a class="bd_edit" style="cursor: pointer;" onclick="javascript: document.getElementById('has_previous_billing').value = false; disableBillingAddress(false);"></a>
								@endif
								<p><strong>Address</strong></p>
								<p><input name="billing_street" id="billing_street" type="text" placeholder="Street" value="{{old('billing_street', $billingAddress->street)}}"></p>
								<p><input name="billing_city" id="billing_city" type="text" placeholder="City" value="{{old('billing_city', $billingAddress->city)}}"></p>
								<p><input name="billing_postal_code" id="billing_postal_code" type="text" placeholder="Postal Code" value="{{old('billing_postal_code', $billingAddress->postal_code)}}"></p>
								<p><input name="billing_province" id="billing_province" type="text" placeholder="Province" value="{{old('billing_province', $billingAddress->province)}}"></p>
							</div>
							<div class="bd_right">
								<p><strong>Email</strong></p>
								<p>{{$user->email}}</p>
								<p><strong>Phone</strong></p>
								<p>{{$user->phone}}</p>
							</div>
						</div>
						<div class="clearFix"></div>
						<h2>Shipping and Payment Details</h2>
						<div class="bd_left">
						<div class="bd_check">
							<input type="checkbox" name="same_as_billing" id="same_as_billing" onclick="javascript: disableShippingAddress(this);" value="1" {{ old('same_as_billing') == 1 ? 'checked' : '' }}>
							<label for="same_as_billing">Same as Billing Address</label>
						</div>
						<div class="bd_shipping_form">
							<p>
							<input name="shipping_street" id="shipping_street" type="text" placeholder="Street" value="{{old('shipping_street')}}"></p>
							<p><input name="shipping_city" id="shipping_city" type="text" placeholder="City" value="{{old('shipping_city')}}"></p>
							<p><input name="shipping_postal_code" id="shipping_postal_code" type="text" placeholder="Postal Code" value="{{old('shipping_postal_code')}}"></p>
							<p><input name="shipping_province" id="shipping_province" type="text" placeholder="Province" value="{{old('shipping_province')}}"></p>
							<div class="clearFix"></div>
						</div>
					</div>
						<div class="bd_right">
							<p></p>
							<p><input style="margin-top: 1em;" name="card_number" id="card_number" type="text" placeholder="Card Number"></p>
							<p><input name="cvv" id="cvv" type="text" placeholder="CVV"></p>
							<p><input name="expiration_date" id="expiration_date" type="text" placeholder="MM/YY"></p>
							<p>
								<select name="card_type" id="card_type">
									<option value="visa">Visa</option>
									<option value="mastercard">Mastercard</option>
									<option value="amex">American Express</option>
								</select>
							</p>
						</div>
						<div class="clearFix"></div>
						@if(!is_null($billingAddress->street))
							<h2>Last Shipping Addresses</h2>
							@foreach(array_chunk($user->userAddresses->toArray(), 2) as $chunk)
								<div class="bd_address">
									@foreach($chunk as $address)
										<div id="previous_shipping-{{$address['id']}}" class="bd_left" style="cursor: pointer;" onclick="javascript: restoreShippingAddress({{$address['id']}})">
											<input type="hidden" id="previous_street-{{$address['id']}}" name="previous_street-{{$address['id']}}" value="{{$address['street']}}">
											<input type="hidden" id="previous_city-{{$address['id']}}" name="previous_city-{{$address['id']}}" value="{{$address['city']}}">
											<input type="hidden" id="previous_province-{{$address['id']}}" name="previous_province-{{$address['id']}}" value="{{$address['province']}}">
											<input type="hidden" id="previous_postal_code-{{$address['id']}}" name="previous_postal_code-{{$address['id']}}" value="{{$address['postal_code']}}">
											<p>{{$address['street']}}</p>
											<p>{{$address['city'] . '/' . $address['province']}}</p>
											<p>{{$address['country']}}</p>
										</div>
									@endforeach
									<div class="clearFix" style="margin-bottom: 2em;"></div>
								</div>
							@endforeach
						@endif
					</form>
				</div>

				<!--- Billing Details Left side end here -->
				
				<!--- Order Details Right side start here -->


				<div class="orderDetails">
					<h2>Your Order</h2>
					<div class="od_col od_head">
						<span class="od_product"><strong>Product</strong></span>
						<span class="od_qty"><strong>Quantity</strong></span>
						<span class="od_price"><strong>Price</strong></span>
					</div>
					<div class="clearFix"></div>
					@foreach($productsArray as $productArray)
						<div class="od_col">
							<span class="od_product">{{ucwords($productArray['product']->name)}}</span>
							<span class="od_qty">{{$productArray['quantity']}}</span>
							<span class="od_price">${{$productArray['quantity'] * $productArray['product']->price}}</span>
						</div>
						<div class="clearFix"></div>
					@endforeach
					<div class="od_line"></div>
					<div class="cartDetails">
						<span class="od_cart_det">Cart Total</span>
						<span class="od_cart_price">${{$pricing['subtotal']}}</span>
					</div>
					<div class="cartDetails">
						<span class="od_cart_det">GST</span>
						<span class="od_cart_price">${{$pricing['gst-total']}}</span>
					</div>
					<div class="cartDetails">
						<span class="od_cart_det">PST</span>
						<span class="od_cart_price">${{$pricing['pst-total']}}</span>
					</div>
					<div class="cartDetails od_highlight">
						<span class="od_cart_det">Shipping Charges</span>
						<span class="od_cart_price">Free</span>
					</div>
					<div class="cartDetails od_highlight">
						<span class="od_cart_det">Total</span>
						<span class="od_cart_price">${{$pricing['total']}}</span>
					</div>
					<div class="od_line"></div>
					<div class="od_payment_method">
						<h3 class="od_head"><strong>Payment Mehtod</strong></h3>
						<input id="payment_method" type="radio" name="payment_method" checked>
						<label for="payment_method">Online</label>
					</div>
					<div class="od_line"></div>
					<div class="od_delivery_date">
						<h3>Expected Delivery Date</h3>
						<p>{{date('d/m/Y', strtotime(date('d/m/Y') . ' + 7 days'))}} by 8pm</p>
					</div>
					<div class="clearFix"></div>
					<div class="od_place_order">
						<a href="#" onclick="javascript: document.getElementById('order_placement').submit();" class="po_btn">Place Order</a>
					</div>
				</div>

				<!--- Order Details Right side end here -->

				<div class="clearFix"></div>
			</div>
		</div>
		<script>
			function disableShippingAddress(checkbox)
			{
				if (checkbox.checked) {
					document.getElementById('shipping_street').setAttribute('readonly', true);
					document.getElementById('shipping_city').setAttribute('readonly', true);
					document.getElementById('shipping_postal_code').setAttribute('readonly', true);
					document.getElementById('shipping_province').setAttribute('readonly', true);
				} else { 
					document.getElementById('shipping_street').removeAttribute('readonly');
					document.getElementById('shipping_city').removeAttribute('readonly');
					document.getElementById('shipping_postal_code').removeAttribute('readonly');
					document.getElementById('shipping_province').removeAttribute('readonly');
				}
			}

			function disableBillingAddress(disable)
			{
				if (disable || document.getElementById('has_previous_billing').value == "true") {
					document.getElementById('billing_street').setAttribute('readonly', true);
					document.getElementById('billing_city').setAttribute('readonly', true);
					document.getElementById('billing_postal_code').setAttribute('readonly', true);
					document.getElementById('billing_province').setAttribute('readonly', true);
				} else {
					document.getElementById('billing_street').removeAttribute('readonly');
					document.getElementById('billing_city').removeAttribute('readonly');
					document.getElementById('billing_postal_code').removeAttribute('readonly');
					document.getElementById('billing_province').removeAttribute('readonly');
				}
			}

			function restoreShippingAddress(address_id)
			{
				document.getElementById('shipping_street').value = document.getElementById('previous_street-' + address_id).value;
				document.getElementById('shipping_city').value = document.getElementById('previous_city-' + address_id).value;
				document.getElementById('shipping_postal_code').value = document.getElementById('previous_postal_code-' + address_id).value;
				document.getElementById('shipping_province').value = document.getElementById('previous_province-' + address_id).value;
			}

			onload = function() {
				disableShippingAddress(document.getElementById('same_as_billing'));
				disableBillingAddress(false);
			}
		</script>
@endsection
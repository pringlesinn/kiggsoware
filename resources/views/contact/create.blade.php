@extends('layouts.kiggs')

@section('content')

    <div class="col-md">
        <div class="contactPage">
            <div class="contactPage">
            <h1>Have Some Questions?</h1>
            <p class="address">
                <img src="images/icon-globe.jpg" alt="" /> 
                WINNIPEG, Canada : Kiggs O Ware <?php echo(date("d-m-Y")); ?> ,WDD-2019</p>
            <div class="clearFix"></div>
            <div class="contactForm">
                <form action="/contact" method="POST">
              @csrf()
@include('includes.error')
                    <input type="text" name="firstName" placeholder="First Name" value="{{old('firstName')}}">
                    <input type="text" name="lastName" placeholder="Last Name" value="{{old('lastName')}}">
                    <input type="email" name="email" placeholder="What is your email?" value="{{old('email')}}">
                    <textarea rows="4" name="message" placeholder="Your questions..." value="{{old('message')}}"></textarea>
                    <input class="submitBtn" type="submit" name="submit" value="Send Message">
                </form>
            </div>
        </div>
        <div class="clearFix"></div>
    </div>
@endsection
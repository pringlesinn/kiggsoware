@extends('layouts.kiggs')

@section('content')
<!-- Main Banner -->
	<div class="banner">
		<img src="images/hero.jpg" alt="">
	</div>
	<div class="col-md">
		<div class="profile-detail">	
			<div class="left">
				
				<div class="profile-content">
					<div class="title">Profile Details</div>
					<ul class="detail-profile">
						<li>
							<div class="detail-name">First Name</div>
							<div class="detail-type">{{$user['first_name']}}</div>
						</li>
						<li>
							<div class="detail-name">Last Name</div>
							<div class="detail-type">{{$user['last_name']}}</div>
						</li>
						<li>
							<div class="detail-name">Email</div>
							<div class="detail-type">{{$user->email}}</div>
						</li>
						<li>
							<div class="detail-name">Phone</div>
							<div class="detail-type">{{$user->phone}}</div>
						</li>
						<li>
							<div class="detail-name">Address</div>
							<div class="detail-type">
								@if(count($user->userAddresses) > 0)
									{{$user->userAddresses->first()->street}}, 
									{{$user->userAddresses->first()->city}}/{{$user->userAddresses->first()->province}}, 
									{{strtoupper($user->userAddresses->first()->postal_code)}},
									{{$user->userAddresses->first()->country}}	
								@else
								No address added yet
								@endif
							</div>
						</li>
						
					</ul>
				</div>
				<a href="/profile/edit">Edit Profile</a>
			</div>
		
			<div class="right">
				<section class="recent-order">
					<div class="widget-title">Previous Orders</div>	
					<ul>
						@if(!is_null($orders))
						@foreach($orders as $order)
						<li>
							<a href="/order_details/{{$order['id']}}">{{$order['user_name']}}-{{$order['created_at']}}</a>
						</li>
						@endforeach
						@else
						No orders Yet!
						@endif
					</ul>
				</section>
			</div>

		</div>		
		
	</div>

	
		

@endsection

 @extends('layouts.kiggs')
 @section('content')

@include('includes.error')
@include('includes.flash')

<div id="container">

  <h1>Update user Profile</h1>
        <form action="/profile/update" method="POST" novalidate>
        	@csrf
			     @method('put')
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="first_name" name="first_name" type="text" value="{{old('first_name', $user->first_name)}}" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                  <label for="first_name">First name</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="last_name" name="last_name" type="text" value="{{old('last_name', $user->last_name)}}" class="form-control" placeholder="Last name" required="required">
                  <label for="last_name">Last name</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input id="email" name="email" type="text" value="{{old('email', $user->email)}}" class="form-control" placeholder="Email address" disabled>
              <label for="email">Email address</label>
            </div>
          </div>
          <div class="form-group">
              <div class="form-label-group">
                  <input id="phone" name="phone" type="text" value="{{old('phone', $user->phone)}}" class="form-control" placeholder="Phone">
                  <label for="phone">Phone</label>
              </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="street" name="street" type="text" value="{{old('street', $user_address->street)}}" class="form-control" placeholder="Street" required="required" autofocus="autofocus">
                  <label for="street">Street</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="city" name="city" type="text" value="{{old('city', $user_address->city)}}" class="form-control" placeholder="City" required="required">
                  <label for="city">City</label>
                </div>
              </div>
            </div>
          </div>
         <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="postal_code" name="postal_code" type="text" value="{{old('postal_code', $user_address->postal_code)}}" class="form-control" placeholder="Postal Code" required="required" autofocus="autofocus">
                  <label for="postal_code">Postal Code</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="province" name="province" type="text" value="{{old('province', $user_address->province)}}" class="form-control" placeholder="Province" required="required">
                  <label for="province">Province</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
             <div class="form-row">
          <button class="btn btn-primary btn-lg">Update Profile</button>
          </div>
          </div>
          
        </form>
</div>        
   
@endsection  
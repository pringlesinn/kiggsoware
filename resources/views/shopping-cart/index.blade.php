@extends('layouts.kiggs')

@section('content')

    <!-- Main Banner -->
        <div class="banner">
            <img src="images/shopping-cart/banner.jpg" alt="">
        </div>
        <!-- //Main Banner -->
        <!-- Main Content -->
        <div class="col-md">
        <div id="shopping-cart" class="mainContent background-yellow">
            <h1 class="color">Shopping Cart</h1>
            <!-- col Content -->
            @if(count($productsArray) > 0)
                <div class="colOne">
                    @foreach($productsArray as $productArray)
                    <div class="shopping-cart-item callout-border">
                        <a href="#" class="remove-item" onclick="javascript:document.getElementById('remove-item-{{$productArray['product']->id}}').submit();">x</a>
                        <form id="remove-item-{{$productArray['product']->id}}" action="/shopping-cart/delete-product/{{$productArray['product']->id}}" style="display: none;" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                        <div class="callout1-img">
                            <img src="/images/uploaded/{{$productArray['product']->image}}" alt="{{$productArray['product']->name}}">
                        </div>
                        <div class="callout1-info">
                            <h3>{{$productArray['product']->name}}</h3>
                            <p>{{$productArray['product']->description}}</p>
                            <p>Sold by: {{$productArray['product']->supplier->name}}</p>
                            <p>Quantity</p>
                            <form action="/shopping-cart/subtractone/{{$productArray['product']->id}}" method="POST">
                                @csrf
                                <input type="submit" value="-" class="minus" id="min" {{($productArray['quantity'] <= 1) ? 'disabled' : ''}}>
                            </form>
                            <form id="update-item-{{$productArray['product']->id}}" action="/shopping-cart/update/{{$productArray['product']->id}}" method="POST">
                                @method('PUT')
                                @csrf
                                <input name="quantity" 
                                       id="quantity" 
                                       type="text" 
                                       value="{{$productArray['quantity']}}" 
                                       class="qty"
                                       onblur="javascript:document.getElementById('update-item-{{$productArray['product']->id}}').submit();">
                            </form>
                            <form action="/shopping-cart/addone/{{$productArray['product']->id}}" method="POST">
                                @csrf
                                <input type="submit" value="+" class="plus" id="plus">
                            </form>
                            <h3>Price: ${{$productArray['product']->price}} / each</h3> 
                        </div>
                        <div class="clearFix margin-btw-callout"></div>
                    </div>
                    @endforeach
                </div>

                <div class="margin-btw-callout"></div>

                <!-- col Content -->
                <div class="colTwo">
                    <div class="callout-border-right">
                        <h3>Payment Method</h3>
                        <img src="/images/shopping-cart/credit-card.png">
                        <div class="callout-table">
                            <table>
                                <tr>
                                    <th>Sub Total</th>
                                    <td>${{$pricing['subtotal']}}</td>
                                </tr>
                                <tr>
                                    <th>Delivery Charges</th>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <th>GST</th>
                                    <td>${{$pricing['gst-total']}}</td>
                                </tr>
                                <tr>
                                    <th>PST</th>
                                    <td>${{$pricing['pst-total']}}</td>
                                </tr>
                                <tr>
                                    <th>Total Cost</th>
                                    <td>${{$pricing['total']}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="form-btn">
                        <a href="/checkout">Continue to checkout</a>
                    </div>
                </div>
            @else
                Nothing was added to the card yet
            @endif

            <div class="clearFix"></div>
            <!-- //Row Content -->
        </div>
    </div>
 <!--  -->
@endsection
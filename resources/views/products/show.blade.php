@extends('layouts.kiggs')

@section('pagespecificstyles')

    <link href="/css/jquery.exzoom.css" rel="stylesheet" type="text/css"/>
@stop

@section('content')
		<div class="breadcrumb">
			<a href="/products/category/{{$parentCategorySlug}}">{{ucwords(implode(' ', explode('-', $parentCategorySlug)))}}</a> / 
			<a href="/products/category/{{$parentCategorySlug}}/{{$subcategorySlug}}">{{ucwords(implode(' ', explode('-', $subcategorySlug)))}}</a> / 
			<span>{{ucwords($product->name)}}</span>
		</div>
		<!-- //Breadcrumb -->
		<!-- Main Content -->
		<div id="productDetails" class="productDetails">
			<div class="productImages">
				<div class="exzoom" id="exzoom">
				    <div class="exzoom_img_box">
				        <ul class='exzoom_img_ul'>
				            <li><img src="/images/uploaded/{{$product->image}}"/></li>
				        </ul>
				    </div>
				    <div class="exzoom_nav"></div>
				    <p class="exzoom_btn">
				        <a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>
				        <a href="javascript:void(0);" class="exzoom_next_btn"> > </a>
				    </p>
				</div>
			</div>
			<div class="productDescription">

				<h1>{{$product->name}}</h1>
				<p>Availability Status: 
					@if($product->is_active)
						Available
					@else
						Not Available, sorry for the inconvenience
					@endif
				</p>
				<form action="/products/addtocart/{{$product->id}}" method="POST">
					@csrf
					<p>
						<label for="quantity">Quantity: </label>
						<select class="qty" name="quantity" id="quantity">
							@for($i = 1; $i <= $maxQuantity; $i++)
								<option value="{{$i}}">{{$i}}</option>	
							@endfor
						</select>
					</p>
					<p><strong>&#36;{{ $product->price }}</strong></p>
					<p><button class="addToCart">ADD TO CART</button></p>
				</form>
				<h2>Product Description & Summary</h2>
				<div class="prodDes">
					<p>{{ $product->description }}</p>
				</div>
			</div>
		</div>
		<!-- //Main Content -->
		<div class="clearFix"></div>
@endsection

@section('pagespecificscripts')
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="/js/jquery.exzoom.js"></script>
    <script type="text/javascript">
      $("#exzoom").exzoom({
            autoPlay: false,
        });
    </script>
@stop
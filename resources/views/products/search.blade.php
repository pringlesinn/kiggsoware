@extends('layouts.kiggs')

@section('banner')
<img src="/images/product-list/banner.jpg" alt="">
@endsection
@section('content')
		<!-- Main Content -->
		<div id="product-list" class="mainContent">
			<div class="filters floatLeft">
				<h2>Category</h2>
				<ul>@if(isset($products) && !is_null($products))
					@foreach($categories as $category)
						<li><a href="/products/category/{{$category['path']}}">{{$category['name']}}</a></li>
					@endforeach	
					@endif

					
				</ul>
			</div>
			<div class="products floatLeft">
				
				@if(isset($message))
					<p>{{$message}}</p>
				@endif	
				@if(isset($products) && count($products) > 0)
					@foreach($products as $product)
						<div class="productRow">
								<div class="productItem">
									<a href="/products/{{$product['id']}}">
										<img src="/images/uploaded/{{isset($product['image']) ? $product['image'] : 'no-image.jpg'}}" alt="{{$product['name']}}">
										<h3>{{$product['name']}}</h3>
										<p class="price"><span>$ {{$product['price']}}</span></p>
									</a>
									<p><a href="#" onclick="javascript: document.getElementById('add-to-cart-{{$product['id']}}').submit();" class="yellowButton roundCorners">Add to cart</a>
										<form id="add-to-cart-{{$product['id']}}" action="/shopping-cart/addone/{{$product['id']}}" method="POST" style="display: none;">
											@csrf
										</form></p>
								</div>
							
						</div>
					@endforeach
				@else
					<p>No products found.</p>
				@endif
				
			</div>
		</div>
		<!-- //Main Content -->
		<div class="clearFix"></div>
@endsection
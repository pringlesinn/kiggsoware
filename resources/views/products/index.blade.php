@extends('layouts.kiggs')

@section('banner')
<img src="/images/product-list/banner.jpg" alt="">
@endsection
@section('content')
		<!-- Main Content -->
		<div id="product-list" class="mainContent">
			<div class="filters floatLeft">
				<h2>Category</h2>
				<ul>
					@if(count($subcategories) > 0)
						@foreach($subcategories as $subcategory)
							<li><a class="{{$currentSubcategory == $subcategory['slug'] ? 'current' : ''}}" href="/products/category/{{$parentCategorySlug}}/{{$subcategory['slug']}}">{{ucwords($subcategory['name'])}}</a></li>
						@endforeach
					@else
						<li>No filters</li>
					@endif
				</ul>	
			</div>
			<div class="products floatLeft">
				@if(count($products) > 0)
					@foreach(array_chunk($products->toArray(), 3) as $chunk)
						<div class="productRow">
							@foreach($chunk as $product)
								<div class="productItem">
									<a href="/products/category/{{$parentCategorySlug}}/{{$product['category_slug']}}/{{$product['id']}}">
										<img src="/images/uploaded/{{isset($product['image']) ? $product['image'] : 'no-image.jpg'}}" alt="{{$product['name']}}">
										<h3 class="namePro truncate">{{ucwords($product['name'])}}</h3>
										<p class="price"><span>$ {{$product['price']}}</span></p>
									</a>
									<p><a href="#" onclick="javascript: document.getElementById('add-to-cart-{{$product['id']}}').submit();" class="yellowButton roundCorners">Add to cart</a>
										<form id="add-to-cart-{{$product['id']}}" action="/shopping-cart/addone/{{$product['id']}}" method="POST" style="display: none;">
											@csrf
										</form></p>
								</div>
							@endforeach
						</div>
					@endforeach
				@else
					<div style="text-align: center;
vertical-align: middle;
line-height: 600px; ">
						<p style="margin: 0">No products found for this category.</p>
					</div>
				@endif
			</div>
		</div>
		<!-- //Main Content -->
		<div class="clearFix"></div>
@endsection
@extends('layouts.kiggs')

@section('content')
        
        <!-- Main Banner starts here -->
        <div class="banner">
            <img src="images/img-banner-login.jpg" alt="">
        </div>
        <div class="col-md">
            <div>
            </div>
            <!-- flash mesaages -->
            <div class="loginPage">
                <div class="loginPageLeft">
                    <h2>Log In/Sign In</h2>
                    <img src="images/icon-login.png" alt="">
                    <p>Enter your details to Login.</p>
                </div>
                <div class="loginPageForm">
                    <span>
                     @include('includes.error')
                    @if (session('flash'))
                     <div class="alert alert-danger">
                        {{session('flash')}}
                     </div>
                     @endif
                    </span>
                    <form method="POST" action="{{ route('login') }}">
                            @csrf
                        <input type="text" id="email" name="email" placeholder="Enter your Username">
                        <div class="clearFix"></div>
                        <input type="password" id="password" name="password" placeholder="Enter your Password">
                        <br/>
                        <a href="/password/reset">Forgot Password?</a>
                        <div class="clearFix"></div>
                        <input type="submit" class="loginBtn" name="submit" value="Log In">

                    </form>
                </div>
            </div>
        </div>
        <!-- Main Banner ends here -->
        
@endsection
@extends('layouts.kiggs')

@section('content')

        <!-- Main Banner -->
        <div class="banner">
            <img src="/images/hero.jpg" alt="">
        </div>
        <div class="resetPassword">
            <div class="resetPasswordLeft">
                <h2>Reset Password</h2>
                <img src="/images/icon-reset-password.jpg" alt="">
                <p>To reset password, Enter your Email address, after that check your email to reset Password</p>
            </div>
            <div class="resetPasswordForm">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your Email Address">
                    <div class="clearFix"></div>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <button type="submit" class="resetPasswordBtn">
                        {{ __('Send Link') }}
                    </button>

                </form>
            </div>
        </div>

        
@endsection

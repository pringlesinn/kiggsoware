@extends('layouts.app')

@section('content')
        <!-- Main Banner -->
        <div class="banner">
            <img src="/images/hero.jpg" alt="">
        </div>
        <div class="resetPassword">
            <div class="resetPasswordLeft">
                <h2>Reset Password</h2>
                <img src="/images/icon-reset-password.jpg" alt="">
                <p>To reset password, Enter a new passowrd and confirm the password.</p>
            </div>
            <div class="resetPasswordForm">
                <formmethod="POST" action="{{ route('password.update') }}">
                        @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="clearFix"></div>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    <div class="clearFix"></div>
                    <button type="submit" class="resetPasswordBtn">
                                    {{ __('Reset Password') }}
                    </button>

                </form>
            </div>
        </div>

@endsection
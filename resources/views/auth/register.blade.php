@extends('layouts.kiggs')

@section('content')
        
        <!-- Main Banner starts here-->
        <div class="banner">
            <img src="images/img-banner-login.jpg" alt="">
        </div>
        <!-- Main Banner ends here-->
        <div class="col-md">
            <div>
            </div>
            <!-- flash mesaages -->
            <div class="registerPage">
                <div class="registerPageLeft">
                    <h2>SignUp/Register</h2>
                    <img src="images/icon-register.jpg" alt="">
                    <p>Register to create an account.</p>
                </div>
                        @include('includes.error')
                <div class="registerPageForm">
                     <form method="POST" action="{{ route('register') }}" novalidate>
                        @csrf
                        
                        <input type="text" id="first_name" name="first_name" placeholder="First Name" class="reg_names">
                        <input type="text" id="last_name" name="last_name" placeholder="Last Name" class="reg_names">
                        <div class="clearFix"></div>
                        <input type="email" id="email" name="email" placeholder="Email Address">
                        <div class="clearFix"></div>
                        <input type="phone" id="phone" name="phone" placeholder="Phone Number">
                        <div class="clearFix"></div>
                        <input type="password" id="password" name="password" placeholder="Password">
                        <div class="clearFix"></div>
                        <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                        <div class="clearFix"></div>
                        <div class="clearFix"></div>
                        <input type="submit" class="registerBtn" name="submit" value="Register Now">
                    </form>
                </div>
            </div>
        </div>
@endsection
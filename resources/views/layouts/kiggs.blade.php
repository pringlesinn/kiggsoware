<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KIGGS 'O' WARE | Home</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  MEDIA="(max-width: 767px)" href="/css/mobile.css"/>
    <script src="/js/main.js"></script>
    <!-- page specific styles -->
    @yield('pagespecificstyles')

</head>
<body>
    <div id="main">
        <!-- hamburger menu starts here -->
        <div class="hamburger" onclick="hamburger(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
        <!-- hamburger menu ends here -->
        <!-- Top header -->
        <div class="header">
            <div class="logo">
                <a href="/" alt="Kiggs 'O' Ware">
                    <img src="/images/tranparent_logo.png" alt="">
                </a>
            </div>
            <div class="topNav">
                <ul>

                    <li><a href="/about-us">About</a></li>
                    <li><a href="/contact">Contact us</a></li>
                    <?php if(Auth::user()):?>
                        <li><a href="/profile">Profile</a> / <a href="/logout">Logout</a></li> 
                    <?php else: ?>
                        <li><a href="/login">Login</a> / <a href="/register">Register</a></li>
                    <?php endif; ?>

                </ul>
            </div>
            <div class="clearFix"></div>
            @include('includes.shopping-cart')
            <div class="clearFix"></div>
            <div class="search">
                <form action="/products" method="POST">
                    @csrf
                    <input class="searchTxt" type="text" id="search "name="search" placeholder="Search">
                    <input class="searchBtn" type="submit" name="" value="">
                </form>
            </div>
        </div>
        <!-- //Top header -->
        <div class="clearFix"></div>
        @include('includes.navigation')
        <div class="mainContent">
            @yield('banner')
            @yield('content')
        </div>
        <!-- //Main Content -->
        <div class="clearFix"></div>
        <!-- Footer -->
        <div class="footer">
            <div class="social">
                <a href="#"><img src="/images/icon-fb.jpg" alt=""></a>
                <a href="#"><img src="/images/icon-instagram.jpg" alt=""></a>
                <a href="#"><img src="/images/icon-twitter.jpg" alt=""></a>
            </div>
            <div class="clearFix"></div>
            <div class="copyright">
                <p>Copyright 2019 Kiggsoware Inc. All Rights Reserved</p>
            </div>
            <div class="clearFix"></div>
            <div class="footerNav">
                <ul>
                    <li><a href="/about-us">About</a></li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Privacy</a></li>
                </ul>
            </div>
        </div>
        <!-- //Footer -->
    </div>
<!-- page specific scripts -->
@yield('pagespecificscripts')

</body>
</html>

@extends('layouts.kiggs')

@section('content')
<main class="py-4">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">404 - Page Not Found</div>

                <div class="card-body">
                    <p>The page you are looking for doesn't exist.</p>
                    <p>Please choose any option from the above menu.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
@endsection

@extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Sub Categories</div> 
          <div class="card-body">
          	<p><a href="/admin/sub_categories/create">Add Sub Category</a></p>	
            <div class="table-responsive">

            @include('includes.flash')
            
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Parent Category</th>
          					<th>Sub Category Name</th>
          					<th>Is Active</th>
          					<th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Parent Category</th>
          					<th>Sub Category Name</th>
          					<th>Is Active</th>
          					<th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @if(count($sub_categories)>0)
					           @foreach ($sub_categories as $sub_category)
                      <tr>
                      <td>
                      @foreach($categories as $category)
            						@if($category['id']==$sub_category['parent_category_id'])
            							{{$category['name']}}
            						@endif
            					@endforeach	
            					</td>
                      <td>{{$sub_category['name']}}</td>
            					<td>
                        @if($sub_category['is_active']==1)
                          Yes
                        @else
                          No
                        @endif
                      </td>
            					<td><a href="/admin/sub_categories/{{$sub_category['id']}}">
            					Edit</a></td>
                      
                      </tr>
                      @endforeach
            					@else
            						<tr>
            							<td colspan="4">No sub category added yet!</td>
            						</tr>
            					@endif
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

@extends('layouts.adminlayout')
 @section('content')
<div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Add Sub Category</div>
      <div class="card-body">
        @include('includes.error')
        @include('includes.flash')
        <form action="/admin/sub_categories" method="POST" novalidate>
        	@csrf

        	  <div class="form-group">
    			  <label for="parent_category_id">Add Sub Category</label>
        			  <select class="form-control" id="parent_category_id" name="parent_category_id">

        			    @foreach($categories as $category)
							<option value="{{$category['id']}}">{{$category['name']}}</option>
						@endforeach
        			    
        			  </select>
    		   </div> 
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input id="name" name="name" type="text" value="{{old('name')}}" class="form-control" placeholder="Sub Category Name" required="required" autofocus="autofocus">
	                  <label for="name">Sub Category Name</label>
	                </div>
	          </div>  
              
              <div class="form-group">
    			  <label for="is_active">Active</label>
        			  <select class="form-control" id="is_active" name="is_active">

        			    <option value=1>Yes</option>
						<option value=0>No</option>
        			    
        			  </select>
    		   </div> 
           <div class="form-row">
              <button type="submit" class="btn btn-primary btn-lg">Add Sub Category</button>
            </div>
          </div>
          
        </form>
      </div>
    </div>
  </div>
@endsection  
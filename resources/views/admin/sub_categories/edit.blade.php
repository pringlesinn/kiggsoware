@extends('layouts.adminlayout')
 @section('content')
<div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Update Sub Category</div>
      <div class="card-body">
        @include('includes.error')
        @include('includes.flash')
        <form action="/admin/sub_categories/{{$sub_category['id']}}" method="post">
			@csrf
			@method('PUT')

        	  <div class="form-group">
    			  <label for="parent_category_id">Update Sub Category</label>
        			  <select class="form-control" id="parent_category_id" name="parent_category_id">

        			   @foreach($categories as $category)
							<option value="<?=$category['id']?>"
							<?php 

							if($category['id']==$sub_category['parent_category_id']):?>
								selected
							<?php endif; ?>	
								>{{$category['name']}}</option>

						@endforeach
        			    
        			  </select>
    		   </div> 
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="name" id="name" value="{{ old('name',$sub_category['name'])}}" class="form-control" placeholder="Sub Category Name" required="required" autofocus="autofocus">
	                  <label for="name">Sub Category Name</label>
	                </div>
	          </div>  
              
              <div class="form-group">
    			  <label for="is_active">Active</label>
        			  <select class="form-control" id="is_active" name="is_active">

        			    <option value=1 
						<?php if($sub_category['is_active']==1): ?>
							selected
						>Yes</option>
						<option value=0 <?php else:?> selected <?php endif; ?>>No</option>
        			    
        			  </select>
    		   </div> 
           <div class="form-row">
          <button type="submit" class="btn btn-primary btn-lg">Update</button>
        </div>
          </div>
         
        </form>
      </div>
    </div>
  </div>
@endsection  
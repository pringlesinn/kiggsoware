 @extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
        
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Parent Categories</div> 
          <div class="card-body">
          	<p><a href="/admin/categories/create"> Add Category </a></p>	
            <div class="table-responsive">
            @include('includes.error')
            @include('includes.flash')

              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($categories as $category)
                  <tr>
                    <td>{{$category->name}}</td>
                    <td>
                      <a href="/admin/categories/{{$category['id']}}">Edit</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

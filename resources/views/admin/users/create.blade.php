
 @extends('layouts.adminlayout')
 @section('content')
<div class="container">
	@include('includes.error')
	@include('includes.flash')
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Add a User</div>
      <div class="card-body">
        <form action="/admin/users" method="POST" novalidate>
          @csrf
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="first_name" name="first_name" type="text" value="{{old('first_name')}}" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                  <label for="first_name">First name</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="last_name" name="last_name" type="text" value="{{old('last_name')}}" class="form-control" placeholder="Last name" required="required">
                  <label for="last_name">Last name</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input id="email" name="email" type="email" value="{{old('email')}}" class="form-control" placeholder="Email address">
              <label for="email">Email address</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input id="password" name="password" type="password" class="form-control" placeholder="Password">
                  <label for="password">Password</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input id="phone" name="phone" type="text" value="{{old('phone')}}" class="form-control" placeholder="Phone">
                  <label for="phone">Phone</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
			<label for="is_admin">Admin</label>
			  <select class="form-control" id="is_admin" name="is_admin">

			    <option value=1>Yes</option>
			    <option value=0>No</option>
			    
			  </select>
			</div> 
      <div class="form-row">
              <button class="btn btn-primary btn-lg">Create</button>
          </div>
          </div>
          
        </form>
      </div>
    </div>
  </div>
@endsection  
@extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">
@include('includes.error')
@include('includes.flash')

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- Icon Cards-->
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5">{{$products}} Products in store</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="/admin/products">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5">{{$transactions}} Transactions</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="/admin/transactions">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5">{{$suppliers}} Registered Suppliers</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="/admin/suppliers">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5">{{$orders}} Orders Placed</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="/admin/purchase_orders">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
        </div>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Users</div> 
          <div class="card-body">
          	<a href="/admin/users/create">Create new User</a>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>Full Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Email</th>
                    <th>Full Name</th>
                    <th>Action</th>
                    
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($users as $user)
                  <tr>
                    <td>{{$user->email}}</td>
                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                    <td>
                      <a href="/admin/users/{{$user->id}}">Edit</a> | <a onclick="javascript: document.getElementById('userRemove-{{$user->id}}').submit();" href="#">Delete</a>
                      <form id="userRemove-{{$user->id}}" action="/admin/users/{{$user->id}}" method="POST" style="display: none">
                        @csrf
                        @method('delete')
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
         
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

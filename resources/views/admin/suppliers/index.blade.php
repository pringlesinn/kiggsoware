@extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Suppliers</div> 
          <div class="card-body"> 
            <h1>Suppliers</h1>
            <a href="/admin/suppliers/create" class="btn btn-primary" style="background-color: #3cb371; border-color: transparent; margin-bottom: 10px;">Add a new supplier
            </a>
            <div class="table-responsive">
            @include('includes.flash')

              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Supplier Id</th>
                    <th>Name</th>
                    <th>Company Name</th>
                    <th>Street</th>
                    <th>City</th>
                    <th>Province</th>
                    <th>Country</th>
                    <th>Postal Code</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Supplier Id</th>
                    <th>Name</th>
                    <th>Company Name</th>
                    <th>Street</th>
                    <th>City</th>
                    <th>Province</th>
                    <th>Country</th>
                    <th>Postal Code</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($suppliers as $supplier)
                  <tr>
                    <th scope="row">{{ $supplier->id }}</th>
                    <td>{{ $supplier->name }}</td>
                    <td>{{ $supplier->company_name }}</td>
                    <td>{{ $supplier->street }}</td>
                    <td>{{ $supplier->city }}</td>
                    <td>{{ $supplier->province }}</td>
                    <td>{{ $supplier->country }}</td>
                    <td>{{ $supplier->postal_code }}</td>
                    <td>{{ $supplier->email }}</td>
                    <td>{{ $supplier->phone }}</td>
                    <td>
                      <a href="/admin/suppliers/{{$supplier->id}}/edit" class="btn btn-primary" role="button" aria-pressed="true">Edit
                      </a>&nbsp;
                    <form class="form d-inline form-inline" 
                    action="/admin/suppliers/{{ $supplier->id }}" 
                    method="post">
                        @csrf 
                        @method('DELETE')
                        <button class="btn btn-danger" style="margin-top: 10px; ">Delete</button>
                    </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

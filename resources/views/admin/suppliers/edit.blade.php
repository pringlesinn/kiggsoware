@extends('layouts.adminlayout')
 @section('content')
<div class="container">
  @include('includes.error')
  @include('includes.flash')
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Update a Supplier</div>
      <div class="card-body">
        <form action="/admin/suppliers/{{ $supplier->id }}" method="post">
          @csrf()
          @method('put')
          <input type="hidden" name="id" value="{{ old('id', $supplier->id) }}">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="name" 
                  name="name" value="{{ old('name', $supplier->name) }}" placeholder="Supplier name" required="required" autofocus="autofocus">
                  <label for="name">Supplier name</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="company_name" 
                  name="company_name" value="{{ old('company_name', $supplier->company_name) }}" placeholder="Last name" required="required">
                  <label for="company_name">Company name</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" class="form-control" id="email" 
                name="email" value="{{ old('email', $supplier->email) }}" placeholder="Email address">
              <label for="email">Email address</label>
            </div>
          </div>
          <label for="street">Address</label>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="street" 
                    name="street" value="{{ old('street', $supplier->street) }}" placeholder="Street">
                  <label for="street">Street</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="city" 
                  name="city" value="{{ old('city', $supplier->city) }}" placeholder="Phone">
                  <label for="city">City</label>
                </div>
              </div>
            </div>
          </div>
           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="province" 
                    name="province" value="{{ old('province', $supplier->province) }}" placeholder="Province">
                  <label for="province">Province</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="country" 
                    name="country" value="{{ old('country', $supplier->country) }}" placeholder="Phone">
                  <label for="country">Country</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="postal_code" 
                    name="postal_code" value="{{ old('postal_code', $supplier->postal_code) }}" placeholder="Postal Code">
                  <label for="postal_code">Postal Code</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="phone" 
                    name="phone" value="{{ old('phone', $supplier->phone) }}" placeholder="Phone">
                  <label for="phone">Phone</label>
                </div>
              </div>
            </div>
          </div>
           <div class="form-row">
          <button type="submit" class="btn btn-primary btn-lg">Update</button>
        </div>
          </div>
       
        </form>
      </div>
    </div>
  </div>
@endsection  
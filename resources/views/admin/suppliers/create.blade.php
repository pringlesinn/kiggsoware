@extends('layouts.adminlayout')
 @section('content')
<div class="container">
  @include('includes.error')
  @include('includes.flash')
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Add a Supplier</div>
      <div class="card-body">
        <form action="/admin/suppliers" method="post">
        @csrf()
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" class="form-control" placeholder="Supplier name" required="required" autofocus="autofocus">
                  <label for="name">Supplier name</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="company_name" 
                  name="company_name" value="{{ old('company_name') }}" class="form-control" placeholder="Last name" required="required">
                  <label for="company_name">Company name</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input id="email" name="email" type="email" value="{{old('email')}}" class="form-control" placeholder="Email address">
              <label for="email">Email address</label>
            </div>
          </div>
          <label for="street">Address</label>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="street" 
                    name="street" value="{{ old('street') }}"class="form-control" placeholder="Street">
                  <label for="street">Street</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="city" 
                   name="city" value="{{ old('city') }}" placeholder="Phone">
                  <label for="city">City</label>
                </div>
              </div>
            </div>
          </div>
           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="province" 
                    name="province" value="{{ old('province') }}"class="form-control" placeholder="Province">
                  <label for="province">Province</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="country" 
                    name="country" value="{{ old('country') }}" placeholder="Phone">
                  <label for="country">Country</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="postal_code" 
                    name="postal_code" value="{{ old('postal_code') }}"class="form-control" placeholder="Postal Code">
                  <label for="postal_code">Postal Code</label>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" class="form-control" id="phone" 
                    name="phone" value="{{ old('phone') }}" placeholder="Phone">
                  <label for="phone">Phone</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-row">
          <button type="submit" class="btn btn-primary btn-lg">Add</button>
        </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection  
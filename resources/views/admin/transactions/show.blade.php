

 @extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">
@include('includes.flash')
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Transactions</div> 
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Purcahse Order ID</th>
					<th>Reference No </th>
					<th>User Name</th>
					<th>Email</th>
                  </tr>
                </thead>
                <tfoot>

                	<th>Purcahse Order ID</th>
					<th>Reference No </th>
					<th>User Name</th>
					<th>Email</th>
                  
                </tfoot>
                <tbody>
                  <td>{{$transactions->purchase_order_id}}</td>
					<td>{{$transactions->reference_number}}</td>
					<td>{{$purchaseorder->user->first_name}} {{$purchaseorder->user->last_name}}</td>
					<td>{{$purchaseorder->user->email}}</td>
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

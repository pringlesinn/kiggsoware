@extends('layouts.adminlayout')
 @section('content')
<div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Update Products</div>
      <div class="card-body">
        @include('includes.error')
        @include('includes.flash')
        <form action="/admin/products/<?=$product->id?>" method="POST"  enctype="multipart/form-data" novalidate>
        	@csrf
			@method('put')
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="name" id="name" value="{{ old('name',$product->name)}}" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
	                  <label for="name">Name</label>
	                </div>
	          </div>  
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="price" id="price" value="{{ old('price',$product->price)}}" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
	                  <label for="name">Price</label>
	                </div>
	          </div>  
	          <div class="form-group">
				  <label for="comment">Description</label>
				  <textarea class="form-control" rows="4" id="description" name="description">
					{{ old('description',$product->description)}}</textarea>
			  </div> 
			  <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="quantity" id="quantity" value="{{ old('quantity',$product->quantity)}}" class="form-control" placeholder="Quantity"  autofocus="autofocus">
	                  <label for="quantity">Quantity</label>
	                </div>
	          </div> 
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="file" name="image" id="image" value="{{old('image',$product->image)}}" class="form-control" placeholder="image" autofocus="autofocus">
	                  <label for="image">Image</label>
	                </div>
	          </div> 
	          
	          <div class="checkbox" id="parent">
	              <label for="parent_category_id">Parent Category</label><br/>
	              @foreach($cats as $cat)
				  <label>
				  	<input type="checkbox" id="parent_category_id" class="parent" 
						 onchange="myfun({{$cat['id']}})" name="parent_category_id[]" value="{{$cat->id}}"
							<?php
								
							foreach($categories as $category):

							 if($cat['id']==$category['parent_category_id']): 
							 	 	
							 	?>
								checked
							<?php endif; 
								endforeach;
							?>	
						 >
						 {{$cat->name}}<br/>
							
						@endforeach
				  </label>
			  </div>
			  <div class="checkbox" id="sub">
			  	  <label>Sub Category</label><br/>
				  <label>
				  	@foreach($categories as $category)
						<input type="checkbox" id="category_id" name="category_id" value="{{$category['id']}}" checked>{{$category['name']}}<br/>
						@endforeach
				   </label>
			  </div>
			  <div class="form-group">
    			  <label for="supplier_id">Supplier</label>
        			  <select class="form-control" id="supplier_id" name="supplier_id">
        			  	@foreach($suppliers as $supplier)
							<option value="{{$supplier['id']}}"

								<?php if($supplier['id']==$product->supplier->id): ?>
									selected
								<?php endif; ?>	
								>{{$supplier->name}}
							</option>
						@endforeach
        			  </select>
        	  </div>
        	  <div class="form-group">
    			  <label for="is_active">Active</label>
        			  <select class="form-control" id="is_active" name="is_active">	
        			  	<option value=1 
							<?php if($product->is_active==1): ?>
								selected
							<?php endif; ?>	
						>Yes</option>
						<option value=0 
							<?php if($product->is_active==0): ?>
								selected
							<?php endif; ?>	
						>No</option>
        			  </select>
        	   </div>	    

              
          	<div class="form-row">
          		<button type="submit" class="btn btn-primary btn-lg">Update</button>
       		</div>
          </div>
          
        </form>
      </div>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script type="text/javascript">


var categories=[];

var chkP= document.getElementById('parent');
var chkbx = chkP.getElementsByTagName('input');

for(var i=0;i<chkbx.length;i++){
	if(chkbx[i].checked){
		categories.push(parseInt(chkbx[i].value));
	}
}

	function myfun(val){		
		var index=categories.indexOf(val);
		if(index!=-1){
			categories.splice(index,1);
		}else{
			categories.push(val);
		}
		console.log(categories);
		console.log(categories);

			$.ajaxSetup({
			  headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});

			$.ajax({
			type:"POST",
			url:"/admin/products/update/sub_categories",
			data:{'data':categories},
			success:function(response){
				setSubCategory(response);
			},error:function(xhr,error,status){
				alert(error);
			}
			
		});
	}
	function setSubCategory(response){
		var html = "";
		if(response.length>0){
			html='<label for="category_id">Sub Category</label><br/>'
		for(var i=0;i<response.length;i++){
			html+='<input type="checkbox" id="category_id" name="category_id[]" value="'+response[i].id+'">'+response[i].name+'<br/>'
		}
	
	}
	console.log(html)
		document.getElementById("sub").innerHTML = html;	
	}
	

</script>	

@endsection  
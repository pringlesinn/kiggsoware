
@extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">
        @include('includes.flash')
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Products</div> 
          <div class="card-body">
          	<a href="/admin/products/create">Create new Product</a>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
					<th>Name</th>
					<th>Price</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Action</th>                  
				   </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
					<th>Price</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($products as $product)
                  <tr>
                    <td>{{$product['name']}}</td>
                    <td>{{$product['price']}}</td>
                    <td>{{$product['description']}}</td>
                    <td>{{$product['quantity']}}</td>
                    <td><a href="/admin/products/{{$product['id']}}">Edit</a> / 
						<form action="/admin/products/{{$product['id']}}" method="post">
							@csrf
							@method('DELETE')
						<button>delete</button>
						</form>
					</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

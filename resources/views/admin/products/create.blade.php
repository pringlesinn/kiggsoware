@extends('layouts.adminlayout')
 @section('content')
<div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Add a Product</div>
      <div class="card-body">
        @include('includes.error')
        @include('includes.flash')
        <form action="/admin/products" method="POST"  enctype="multipart/form-data">
        	@csrf
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="name" id="name" value="{{ old('name')}}" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
	                  <label for="name">Name</label>
	                </div>
	          </div>  
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="price" id="price" value="{{ old('price')}}" class="form-control" placeholder="Price" required="required" autofocus="autofocus">
	                  <label for="price">Price</label>
	                </div>
	          </div>  
	          <div class="form-group">
				  <label for="comment">Description</label>
				  <textarea class="form-control" rows="4" id="description" name="description">
					{{ old('description')}}
				  </textarea>
			  </div> 
			  <div class="form-group">
	                <div class="form-label-group">
	                  <input type="text" name="quantity" id="quantity" value="{{ old('quantity')}}" class="form-control" placeholder="Quantity" required="required" autofocus="autofocus">
	                  <label for="quantity">Quantity</label>
	                </div>
	          </div> 
	          <div class="form-group">
	                <div class="form-label-group">
	                  <input type="file" name="image" id="image" value="{{ old('image')}}" class="form-control" placeholder="image" required="required" autofocus="autofocus">
	                  <label for="image">Image</label>
	                </div>
	          </div> 
	          
	          <div class="checkbox" id="parent">
	              <label for="parent_category_id">Parent Category</label><br/>
	              @foreach($categories as $key => $category)
				  <label>
				  	<input type="checkbox" id="parent_category_id"  onchange="myfun({{$category['id']}})" name="parent_category_id[]" 
				  			value="{{$category['id']}}">
				  				{{$category['name']}}
				  			<br/>
				  	</label><br/>
				   @endforeach
				  
			  </div>
			  <div class="checkbox" id="sub">
			  	  
			  </div>
			  <div class="form-group">
    			  <label for="supplier_id">Supplier</label>
        			  <select class="form-control" id="supplier_id" name="supplier_id">
        			  	@foreach($suppliers as $supplier)

							<option value="{{$supplier['id']}}">{{$supplier['name']}}</option>
						@endforeach
        			  </select>
        	  </div>
        	  <div class="form-group">
    			  <label for="is_active">Active</label>
        			  <select class="form-control" id="is_active" name="is_active">	
        			  	<option value=1 > Yes </option>
						<option value=0 > No </option>
        			  </select>
        	   </div>	    

              
          <div class="form-row">
          <button type="submit" class="btn btn-primary btn-lg">Add Product</button>
        </div>
          </div>
         
        </form>
      </div>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script type="text/javascript">
	
	var categories=[];
	var chkP= document.getElementById('parent');
	var chkbx = chkP.getElementsByTagName('input');

	for(var i=0;i<chkbx.length;i++){
		if(chkbx[i].checked){
			categories.push(parseInt(chkbx[i].value));
		}
	}
	function myfun(val){
		var index=categories.indexOf(val);
		if(index!=-1){
			categories.splice(index,1);
		}else{
			categories.push(val);
		}
		console.log(categories);

			$.ajaxSetup({
			  headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});

			$.ajax({
			type:"POST",
			url:"/admin/products/get/sub_categories",
			data:{'data':categories},
			success:function(response){
				setSubCategory(response);
			},error:function(xhr,error,status){
				alert(error);
			}
			
		});
	}
	function setSubCategory(response){
		var html = "";
		if(response.length>0){
			html='<label for="category_id">Sub Category</label><br/>'
		for(var i=0;i<response.length;i++){
			html+='<input type="checkbox" id="category_id" name="category_id[]" value="'+response[i].id+'">'+response[i].name+'<br/>'
		}
	
	}
	console.log(html)
		document.getElementById("sub").innerHTML = html;	
	}
	

</script>
	

</script>	

@endsection  
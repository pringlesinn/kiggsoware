@extends('layouts.adminlayout')
 @section('content')
<div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Update Order</div>
      <div class="card-body">
        @include('includes.error')
        @include('includes.flash')
        <form action="/admin/purchase_orders/{{$order->id}}" method="POST">
		@csrf

        @method('PUT') 
              
              <div class="form-group">
    			  <label for="is_delivered">Delivery Status</label>
        			  <select class="form-control" id="is_delivered" name="is_delivered">

        			    <option <?php if($order->is_delivered==1):?> selected <?php endif; ?> value=1 >Delivered</option>
						<option <?php if($order->is_delivered==0): ?>selected <?php endif; ?> value=0 >Pending</option>
        			    
        			  </select>
    			</div>
          <div class="form-row">
          <button type="submit" class="btn btn-primary btn-lg">Update</button>
        </div> 
          </div>
          
        </form>
      </div>
    </div>
  </div>
@endsection  
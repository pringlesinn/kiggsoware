@extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Orders</div> 
          <div class="card-body">	
            <h1>View orders of Users</h1>
            <div class="table-responsive">
            @include('includes.flash')

              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>User Name</th>
					<th>User Email</th>
					<th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>User Name</th>
					<th>User Email</th>
					<th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                 @foreach($purchase_orders as $po)
					<tr>
						<td>{{$po->user_name}}</td>
						<td>{{$po->user_email}}</td>
						<td><a href="/admin/purchase_orders/{{ $po->user_id }}">View</a></td>		
					</tr>
				@endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection

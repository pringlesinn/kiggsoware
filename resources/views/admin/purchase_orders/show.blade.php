@extends('layouts.adminlayout')
 @section('content')
 <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/admin/users">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Orders</div> 
          <div class="card-body">
  	
            <div class="table-responsive">
            @include('includes.flash')
	            <ul class="sidebar navbar-nav">
		            <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          <span>Filter Orders</span>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
				          <h6 class="dropdown-header">Transaction Status</h6>
				          <a class="dropdown-item" href="/admin/purchase_orders/{{$user_id}}/pending">Pending</a>
				          <a class="dropdown-item" href="/admin/purchase_orders/{{$user_id}}/completed">Completed</a>
				          <div class="dropdown-divider"></div>
				          <h6 class="dropdown-header">Delivery Status</h6>
				          <a class="dropdown-item" href="/admin/purchase_orders/{{$user_id}}/delivery_pending">Pending</a>
				          <a class="dropdown-item" href="/admin/purchase_orders/{{$user_id}}/delivery_delivered">Delivered</a>
				        </div>
				    </li>
			    </ul>
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>User Name</th>
					<th>User Email</th>
					<th>Shipping Address</th>
					<th>Billing Address</th>
					<th>Sub Total</th>
					<th>GST</th>
					<th>PST</th>
					<th>Extimated Delivery Date</th>
					<th>Delivery Status</th>
					<th>Delivery Date</th>
					<th>Payment Status</th>
					<th>Total</th>
					<th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>User Name</th>
					<th>User Email</th>
					<th>Shipping Address</th>
					<th>Billing Address</th>
					<th>Sub Total</th>
					<th>GST</th>
					<th>PST</th>
					<th>Extimated Delivery Date</th>
					<th>Delivery Status</th>
					<th>Delivery Date</th>
					<th>Payment Status</th>
					<th>Total</th>
					<th>Action</th>
                  </tr>
                </tfoot>
                @if(count($purchase_orders)>0)
					@foreach($purchase_orders as $purchase_order)
					<tr>

					<td>{{$purchase_order->user_name}}</td>

					<td>{{$purchase_order->user_email}}</td>

					<td><?php $shipping_address = json_decode($purchase_order->shipping_address_json); ?>
						{{$shipping_address->address}}</td>
					<td><?php $billing_address = json_decode($purchase_order->billing_address_json); ?>
						{{$billing_address->address}}</td>	

					<td>{{$purchase_order->sub_total}}</td>

					<td>{{$purchase_order->gst}}</td>

					<td>{{$purchase_order->pst}}</td>

					<td>{{$purchase_order->estimated_delivery}}</td>

					<td>@if($purchase_order->is_delivered==1)
							Deliverd
						@else
							Pending
						@endif	
					</td>

					<td>{{$purchase_order->delivery_date}}</td>

					<td>{{$purchase_order->payment_status}}</td>

					<td>{{$purchase_order->total}}</td>
					<td><a href="/admin/purchase_orders/update/{{$purchase_order->id}}">Update status</a></td>
					</tr>
					@endforeach
					@else
					<tr>
						<td>No result found</td>
					</tr>
					@endif
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->
@endsection
 
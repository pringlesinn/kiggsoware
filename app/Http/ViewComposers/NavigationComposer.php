<?php 

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Category;
use App\Helper\ShoppingCart;

class NavigationComposer
{
    public $navigationOptions = [];
    public $shoppingCartItemCount;

    /**
     * Create a navigation composer.
     *
     * @return void
     */
    public function __construct()
    {
        $shoppingCart = new ShoppingCart();
        $this->navigationOptions = Category::findActive();
        $this->shoppingCartItemCount = $shoppingCart->getCountItems();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('navigationOptions', end($this->navigationOptions));
        $view->with('shoppingCartItemCount', $this->shoppingCartItemCount);
    }

}
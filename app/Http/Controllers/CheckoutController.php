<?php

namespace App\Http\Controllers;

use App\Helper\PaymentTransaction;
use App\Helper\ShoppingCart;
use App\Product;
use App\PurchaseOrder;
use App\Transaction;
use App\UserAddress;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function index()
    {
    	if (!Auth::user()) {
    		return redirect('/login?checkout=true');
    	}

    	if (ShoppingCart::getCountItems() < 1) {
    		return redirect('/');
    	}

    	$user = Auth::user();
    	if (count($user->userAddresses) > 0) {
    		$billingAddress = $user->userAddresses[0];
    	} else {
    		$billingAddress = new UserAddress();
    	}

    	$productsArray = ShoppingCart::getShoppingCartProducts();
		$pricing['subtotal'] = ShoppingCart::getTotalWithoutTaxes();
		$pricing['gst-total'] = ShoppingCart::getGSTForAllProducts();
		$pricing['pst-total'] = ShoppingCart::getPSTForAllProducts();
		$pricing['total'] = ShoppingCart::getTotalWithTaxes();


    	return view('checkout.index', compact('user', 'billingAddress', 'productsArray', 'pricing'));
    }

    public function store()
    {
    	$validBilling = request()->validate([
			'billing_street' => 'required|max:255',
			'billing_city' => 'required|max:255',
			'billing_postal_code' => 'required|regex:/^([a-zA-Z]{1}\d{1}[a-zA-Z]{1})\s?(\d{1}[a-zA-Z]{1}\d{1})$/',
			'billing_province' => 'required']);

    	if (is_null(request()->get('same_as_billing'))) {
    		$validShipping = request()->validate([
    		'shipping_street' => 'required|max:255',
			'shipping_city' => 'required|max:255',
			'shipping_postal_code' => 'required|regex:/^([a-zA-Z]{1}\d{1}[a-zA-Z]{1})\s?(\d{1}[a-zA-Z]{1}\d{1})$/',
			'shipping_province' => 'required']);
    	} else {
    		$validShipping = array(
    			'shipping_street' => $validBilling['billing_street'],
				'shipping_city' => $validBilling['billing_city'],
				'shipping_postal_code' => $validBilling['billing_postal_code'],
				'shipping_province' => $validBilling['billing_province']
    		);
    	}
    	$validBilling['address'] = $validBilling['billing_street'] . ', ' . $validBilling['billing_city'] . '/' . $validBilling['billing_province'] . ', ' . $validBilling['billing_postal_code'];
    	$validShipping['address'] = $validShipping['shipping_street'] . ', ' . $validShipping['shipping_city'] . '/' . $validShipping['shipping_province'] . ', ' . $validShipping['shipping_postal_code'];

    	$validPayment = request()->validate([
    		'card_number' => 'required|integer|regex:/^[\d]{16}$/',
    		'cvv' => 'required|integer|regex:/^[\d]{3}$/',
    		'expiration_date' => 'required|regex:/^[\d]{1}[0-2]{1}\/?[\d]{2}$/'

    	]);

    	$user = Auth::user();    	

    	// First we save the Purchase Order
    	$purchaseOrder = new PurchaseOrder();
    	$purchaseOrder['user_id'] = $user->id;
    	$purchaseOrder['user_name'] = $user->first_name . ' ' . $user->last_name;
    	$purchaseOrder['user_email'] = $user->email;
    	$purchaseOrder['shipping_address_json'] = json_encode($validShipping);
    	$purchaseOrder['billing_address_json'] = json_encode($validBilling);
    	$purchaseOrder['sub_total'] = ShoppingCart::getTotalWithoutTaxes();
    	$purchaseOrder['pst'] = ShoppingCart::getPSTForAllProducts();
    	$purchaseOrder['gst'] = ShoppingCart::getGSTForAllProducts();
    	$purchaseOrder['total'] = ShoppingCart::getTotalWithTaxes();
    	$purchaseOrder['estimated_delivery'] = Carbon::now()->addDays(7);
    	$purchaseOrder->save();
    	// Then we add products to this order
    	foreach (ShoppingCart::getShoppingCartProducts() as $productArray) {

    		$product = $productArray['product'];

    		$purchaseOrder->products()->attach(Product::find($product->id), ['unit_price' => $product->price, 'quantity' => $productArray['quantity']]);
    	}

    	if (count($user->userAddresses) > 0) {
    		$billingAddress = $user->userAddresses->first();
    		$billingAddress->street = $validBilling['billing_street'];
			$billingAddress->city = $validBilling['billing_city'];
			$billingAddress->postal_code = $validBilling['billing_postal_code'];
			$billingAddress->province = $validBilling['billing_province'];

			$billingAddress->update();
    	} else {
	    	UserAddress::create([
	    		'user_id' => $user->id,
	    		'street' => $validBilling['billing_street'],
	    		'city' => $validBilling['billing_city'],
	    		'postal_code' => $validBilling['billing_postal_code'],
	    		'province' => $validBilling['billing_province'],
	    		'country' => 'Canada'
	    	]);
    	}

    	if (is_null(request()->get('same_as_billing'))) {
	    	UserAddress::create([
	    		'user_id' => $user->id,
	    		'street' => $validShipping['shipping_street'],
	    		'city' => $validShipping['shipping_city'],
	    		'postal_code' => $validShipping['shipping_postal_code'],
	    		'province' => $validShipping['shipping_province'],
	    		'country' => 'Canada'
	    	]);
    	}
    	
    	//do card transaction
    	$paymentInformation['amount'] = ShoppingCart::getTotalWithTaxes();
    	$paymentInformation['card_num'] = request()->get('card_number');
    	$paymentInformation['exp_date'] = request()->get('expiration_date');
    	$paymentInformation['cvv'] = request()->get('cvv');;
    	$paymentInformation['ref_num'] = 'K'. rand() . rand() . 'W';
    	$paymentInformation['card_type'] = request()->get('card_type');
    	
    	$response = PaymentTransaction::process($paymentInformation);
    	$transaction = new Transaction();
    	$transaction['purchase_order_id'] = $purchaseOrder->id;
    	$transaction['reference_number'] = $paymentInformation['ref_num'];
    	$transaction['response_json'] = json_encode($response);

    	$transaction->save();

    	$response = json_decode($transaction->response_json)->transaction_response;
		if ($response->response_code == "1" && count($response->errors) == 0) {
			$purchaseOrder['payment_status'] = 'completed';
			$purchaseOrder->update();
		}
		
    	//clear cart
    	ShoppingCart::clearCart();
    	
    	return redirect("/order_details/$purchaseOrder->id");

    }
}

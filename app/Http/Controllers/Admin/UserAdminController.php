<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Helper;
use App\Mail\SendMailable;
use App\User;
use App\Transaction;
use App\Supplier;
use App\PurchaseOrder;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class UserAdminController extends Controller
{
    public function index()
    {
        $transactions = Transaction::all()->count();
        $suppliers= Supplier::all()->count();
        $orders=PurchaseOrder::all()->count();
        $products=Product::all()->count();

    	$users = User::all();
    	return view('admin.users.index', compact('users','transactions','suppliers','orders','products')); 
    }

    public function create()
    {
    	return view('admin.users.create');
    }

    public function store()
    {
        $valid = request()->validate([
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|max:191|regex:/[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/',//CHECKED with gmail,don't allow anyting except "."
            'password' => 'required|min:8|max:191|regex:/(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[\d]+)(?=.*[\!\@\#\$\%\^\&\*\(\)\{\}\[\]\?\/\'\"]+).{8,}/',
            'phone' => 'required|regex:/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/',
            'is_admin'=>'required|boolean'
        ]);

        $valid['password'] = password_hash($valid['password'], PASSWORD_DEFAULT);
        $valid['created_at'] = Carbon::now();
        $valid['updated_at'] = Carbon::now();

        $flashMessage = Helper::generateFlashMessage(User::create($valid), 'create', 'user');

        return redirect('admin/users')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function destroy($id)
    {
        $flashMessage = Helper::generateFlashMessage(User::find($id)->delete(), 'delete', 'user');

    	return redirect('/admin/users')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function edit(User $user)
    {
    	return view('admin.users.edit', compact('user'));
    }

    public function update($id)
    {
        $valid = request()->validate([
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|max:191||regex:/[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/',
            'phone' => 'required|regex:/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/',
            'is_admin' => 'required|boolean' 
        ]);

        $user = User::findOrFail($id);
        $user->first_name = $valid['first_name'];
        $user->last_name = $valid['last_name'];
        $user->email = $valid['email'];
        $user->phone = $valid['phone'];
        $user->is_admin = $valid['is_admin'];
        $user->updated_at = Carbon::now();

        $flashMessage = Helper::generateFlashMessage($user->save(), 'update', 'user');

        return redirect('/admin/users')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function resetPassword($id)
    {
    	$user = User::findOrFail($id);
    	$params['name'] = $user->first_name . ' ' . $user->last_name;
    	$params['to'] = $user->email;
    	Mail::to('contact@kiggsoware.com')->send(new SendMailable('emails.password-reset', 'Password reset', $params));

    	return redirect('/admin/users')->with('success', 'Password has been reseted!');
    }


}

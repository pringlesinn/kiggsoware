<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Helper;
use App\Supplier;
use Carbon\Carbon;

class SupplierAdminController extends Controller
{
    public function index()
    
    {
        $slug = 'suppliers';
        $suppliers = Supplier::all();
        return view('admin.suppliers.index', compact('slug','suppliers'));
    }

    public function create()
    {
        return view('admin.suppliers.create');
    }

    public function store(Request $request)
    {
        $valid = request()->validate([
            "name" => 'required|max:191',
            "company_name" => 'required|max:191',
            "street" => 'required|max:191',
            "city" => 'required|max:191',
            "province" => 'required|max:191',
            "country" => 'required|max:191',
            "postal_code" => 'required|max:191|regex:/^([a-zA-Z]{1}\d{1}[a-zA-Z]{1})\s?(\d{1}[a-zA-Z]{1}\d{1})$/',
            "email" => 'required|email|max:191|regex:/[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/',
            "phone" => 'required|regex:/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/'
        ]);
        $supplier = new Supplier();
        $supplier->name = request('name');
        $supplier->company_name = request('company_name');
        $supplier->street = request('street');
        $supplier->city = request('city');
        $supplier->province = request('province');
        $supplier->country = request('country');
        $supplier->postal_code = str_replace(' ','',request('postal_code'));
        $supplier->email = request('email');
        $supplier->phone = request('phone');
        $supplier->created_at = Carbon::now();
        $supplier->updated_at = Carbon::now();

        $flashMessage = Helper::generateFlashMessage($supplier->save(), 'create', 'supplier');

        return redirect('/admin/suppliers')->with($flashMessage['type'], $flashMessage['message']);
    }


    public function edit($id)
    {
        $supplier = Supplier::find($id);
        return view('admin.suppliers.edit',compact('supplier'));
    }

    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $valid = request()->validate([
            "name" => 'required|max:191',
            "company_name" => 'required|max:191',
            "street" => 'required|max:191',
            "city" => 'required|max:191',
            "province" => 'required|max:191',
            "country" => 'required|max:191',
            "postal_code" => 'required|max:191|regex:/^([a-zA-Z]{1}\d{1}[a-zA-Z]{1})\s?(\d{1}[a-zA-Z]{1}\d{1})$/',
            "email" => 'required|email|max:191|regex:/[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/',
            "phone" => 'required|regex:/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/'
        ]);

        $supplier->name = request('name'); 
        $supplier->company_name = request('company_name');
        $supplier->street = request('street');
        $supplier->city = request('city');
        $supplier->province = request('province');
        $supplier->country = request('country');
        $supplier->postal_code = str_replace(' ','',request('postal_code'));
        $supplier->email = request('email');
        $supplier->phone = request('phone');
        $supplier->created_at = Carbon::now();   
        $supplier->updated_at = Carbon::now();
        
        $flashMessage = Helper::generateFlashMessage($supplier->save(), 'update', 'supplier');

        return redirect('/admin/suppliers')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        
        $flashMessage = Helper::generateFlashMessage($supplier->delete(), 'delete', 'supplier');

        return redirect('/admin/suppliers')->with($flashMessage['type'], $flashMessage['message']);
    }

}

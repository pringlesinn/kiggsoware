<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PurchaseOrder;
use App\Helper\Helper;


class PurchaseOrderAdminController extends Controller
{
    public function index()
    {
    	$title= 'List View';
    	$purchase_orders = PurchaseOrder::all();

    	return view('admin.purchase_orders.index', compact('purchase_orders','title'));
    }

    public function show($user_id)
    {
    	$title= 'Detail View';
    	$purchase_orders = PurchaseOrder::where('user_id',$user_id)->get();
    	return view('admin.purchase_orders.show', compact('purchase_orders','user_id', 'title'));
    }

    public function edit($id)
    {
        $title = "Update status view";
        $order = PurchaseOrder::findOrFail($id);
        return view('admin.purchase_orders.edit',compact('order'));
    }

    public function update($id)
    {
        $title = "List View";
        $purchaseorder = PurchaseOrder::findOrFail($id); 
        $valid = request()->validate([
            'is_delivered' => 'required|boolean'
        ]);

        $flashMessage = Helper::generateFlashMessage($purchaseorder->update($valid), 'update', 'delivery status');

        return redirect("/admin/purchase_orders/$purchaseorder->user_id")->with($flashMessage['type'], $flashMessage['message']);
    }

    public function status($id,$status)
    {
        if($status =='pending'||$status =='completed'){
            $title="Search result for ".$status;
            $user_id=$id;
            $matchThese = ['user_id' => $id, 'payment_status' => $status];
            $purchase_orders = PurchaseOrder::where($matchThese)->get();
           
        }else{
            $title="Result for search is:";
            $user_id= $id;
            if($status=='delivery_pending'){
                $status=0;
            }else{
                $status=1;
            }
            $matchThese=['user_id'=>$id,'is_delivered'=>$status];
            $purchase_orders = PurchaseOrder::where($matchThese)->get();
        }    

        return view("admin.purchase_orders.show",compact('purchase_orders','title','user_id'));
    }
}

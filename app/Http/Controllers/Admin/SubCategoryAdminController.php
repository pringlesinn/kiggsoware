<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\RemovedData;
use App\Helper\Helper;
use Exception;
use Str;

class SubCategoryAdminController extends Controller
{
	/**
	 * List view of Sub Categories
	 * @return array
	 */
    public function index()
    {
    	$categories = Category::whereNull('parent_category_id')->get();
    	$sub_categories = Category::whereNotNull('parent_category_id')->get();
    	return view('admin.sub_categories.index',compact('sub_categories','categories'));
    }
    /**
     * View to show form to add Sub category
     * @return array
     */
    public function create()
    {
    	$categories= Category::whereNull('parent_category_id')->get();
    	return view('admin.sub_categories.create',compact('categories'));
    }
    /**
     * Function to add new sub category
     * @return array
     */
    public function store()
    {
    	
    	$valid = request()->validate([
    		'parent_category_id'=>'required|integer',
    		'name'=>'required|unique_with:categories,parent_category_id,name|max:191',
    		'is_active'=>'required|boolean'
    	]);
        $valid['slug'] = Str::slug($valid['name']);

    	$flashMessage = Helper::generateFlashMessage(Category::create($valid), 'create', 'Sub Category');
        
        return redirect('/admin/sub_categories')->with($flashMessage['type'], $flashMessage['message']);
    }

    /**
     * Function to show edit view of a subcategory
     * @param   $id 
     * @return array
     */
    public function edit($id)
    {
    	$sub_category = Category::findOrFail($id);
    	$categories = Category::whereNull('parent_category_id')->get();
    	return view('admin.sub_categories.edit',compact('sub_category','categories'));
    }

    /**
     * Updating an existing sub category
     * @param  $id
     * @return array
     */
    public function update($id)
    {
    	$sub_category = Category::findOrFail($id);
    	$valid = request()->validate([
    		'parent_category_id'=>'required|integer',
    		'name'=>'required|unique_with:categories,parent_category_id,name|max:191',
    		'is_active'=>'required|boolean'
    	]);
        $valid['slug'] = Str::slug($valid['name']);

    	$flashMessage = Helper::generateFlashMessage($sub_category->update($valid), 'update', 'Sub Category');
        
        return redirect('/admin/sub_categories')->with($flashMessage['type'], $flashMessage['message']);
    }

}


<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Helper\Helper;
use Carbon\Carbon;
use Str;

class CategoryAdminController extends Controller
{
    public function index()
    {
    	$title = 'List View';
    	$categories= Category::whereNull('parent_category_id')->get();
    	return view('admin.categories.index',compact('categories','title'));
    }  

    public function create()
    {
       return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $valid = request()->validate([
            "name" => 'required|unique_with:categories,parent_category_id,name',
            "is_active"=>'required|boolean'
        ]);

        $categories=Category::whereNull('parent_category_id')->where('is_active',1)->get();
        if(count($categories)>=6 && request('is_active')==1){
            $flashMessage['type']='error';
            $flashMessage['message'] = 'Already 6 categories are active. If you want to activate this category, please de-activate another category';
            return redirect('/admin/categories')->with($flashMessage['type'], $flashMessage['message']);
        }

        $add_category = new Category();
        $add_category->name = request('name');
        $add_category->is_active = request('is_active');
        $add_category->slug = Str::slug($add_category->name);
        $flashMessage = Helper::generateFlashMessage($add_category->save(), 'create', 'category');

        return redirect('/admin/categories')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function edit($id)
    {
      $editCategory = Category::find($id);
      return view('admin.categories.edit',compact('editCategory'));
    }

    public function update(Request $request, $id)
    {
        $update_category = Category::find($id);
        $valid = request()->validate([
            "name" => 'required|unique:categories,parent_category_id,name'.$id,
            "is_active"=>"required|boolean"
        ]);

        $categories=Category::whereNull('parent_category_id')->where('is_active',1)->get();
        if(count($categories)>=6 AND request('is_active')==1){
            $flashMessage['type']='error';
            $flashMessage['message'] = 'Already 6 categories are active. If you want to activate this category, please de-activate another category';
            return redirect('/admin/categories')->with($flashMessage['type'], $flashMessage['message']);
        }

        $update_category->name = request('name');
        $update_category->is_active=request('is_active');
        $update_category->slug = Str::slug($update_category->name);
        $update_category->updated_at = Carbon::now();
        $flashMessage = Helper::generateFlashMessage($update_category->save(), 'update', 'category');

        return redirect('/admin/categories')->with($flashMessage['type'], $flashMessage['message']);
    }

}

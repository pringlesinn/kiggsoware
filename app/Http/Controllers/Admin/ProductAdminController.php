<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Category;
use App\Supplier;
use App\Helper\Helper;
use App\Product;
use \Carbon\Carbon;

class ProductAdminController extends Controller
{
	/**
	 * To view the list of products
	 * @return array List of products
	 */
    public function index()
    {
    	$products= Product::get();
    	return view('admin.products.index',compact('products'));
    }

    /**
     * Fetching product data in the edit form
     * @param  Interger $id 
     * @return arrays
     */
    public function edit($id)
    {
    	$product= Product::findOrFail($id);
        $cats= Category::whereNull('parent_category_id')->get();
        $suppliers= Supplier::all();
        $categories=$product->categories()->get();
    	return view('admin.products.edit',compact('product','categories','cats','suppliers'));
    }

    /**
     * To show the view of adding product
     * @return arrays
     */
    public function create()
    {
        $categories= Category::whereNull('parent_category_id')->where('is_active',1)->get();
        $suppliers= Supplier::all();
        return view('admin.products.create',compact('categories','suppliers'));
    }

    /**
     * Adding product to the database
     * @return string 
     */
    public function store(Request $request)
    {

        $product= new Product;
       
        $products= request()->validate([
            'name'=>'required|max:191',
            'price' => 'required|regex:/^[0-9]+([.][0-9]+)?$/',
            'quantity' => 'required|integer',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'description' => 'required|min:14|max:4000',
            'is_active' => 'required|boolean',
            'supplier_id'=>'required|integer',
            'parent_category_id'=>'required',
            'category_id'=>'required' 
        ]);
        
        $image = time().'.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('images/uploaded'), $image);
        
        $product->name = request('name');
        $product->price = request('price');
        $product->quantity = request('quantity');
        $product->image = $image;
        $product->description = request('description');
        $product->is_active = request('is_active');
        $product->supplier_id = request('supplier_id');
        $pro = $product->save();
        
        if($pro){
         $flashMessage = Helper::generateFlashMessage($product->categories()->sync($request->category_id), 'create', 'product');
        }else{
            $flashMessage['type']="error";
            $flashMessage['message']="Problem in saving data";
        }
        return redirect('/admin/products')->with($flashMessage['type'], $flashMessage['message']);

    }


    /**
     * Fucntion to update the product
     * @param  Integer  $id     
     * @return 
     */
    public function update(Request $request,$id)
    {

        $product = Product::find($id);

    	$valid= request()->validate([
    	 	'name'=>'required|max:191',
    	 	'price' => 'required|regex:/^[0-9]+([.][0-9]+)?$/',
    	 	'quantity' => 'required|integer',
    	 	'description' => 'required|min:14|max:4000',
    	    'is_active' => 'required|boolean',
            'supplier_id'=>'required|integer',
            'parent_category_id'=>'required',
            'category_id'=>'required'
           
    	]);

        if($request->hasFile('image')){
            $valid= request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $image = time().'.'.request()->image->getClientOriginalExtension();

            request()->image->move(public_path('images/uploaded'), $image);
            $product->image =$image;
        }

        $product->name = request('name');
        $product->price = request('price');
        $product->quantity = request('quantity');
        $product->description = request('description');
        $product->is_active = request('is_active');
        $product->supplier_id = request('supplier_id');
        $product->updated_at = Carbon::now();
        
        $pro = $product->save();
        
        if($pro){
         $flashMessage = Helper::generateFlashMessage($product->categories()->sync($request->category_id), 'create', 'product');
        }else{
            $flashMessage['type']="error";
            $flashMessage['message']="Problem in saving data";
        }

        return redirect('/admin/products')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        
        $flashMessage = Helper::generateFlashMessage($product->delete(), 'delete', 'product');
       
        return redirect('/admin/products')->with($flashMessage['type'], $flashMessage['message']);
    }

    public function categories(Request $request){
        $sub_cat = [];
         $category= $request->get('data');
         if(!empty($category)){
            foreach ($category as  $value) {
              $data = Category::where('parent_category_id',$value)->where('is_active',1)->get();
                if(count($data)>0){
                    foreach ($data as $value) {
                        array_push($sub_cat, $value);
                    }      
                }
            }
        }  
        return response()->json($sub_cat);
    }
}



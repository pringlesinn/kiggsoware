<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;
use App\PurchaseOrder;

class TransactionAdminController extends Controller
{
    public function index()
    {
    	$title= 'Transaction Detail View';
    	$transactions = Transaction::all();
    	return view('admin.transactions.index', compact('transactions','title')); 
    }

    public function show($transaction_id)
    {
    	$title= 'Transaction Detail View';
    	$transactions = Transaction::findOrFail($transaction_id);
        $purchaseorder = PurchaseOrder::findOrFail($transactions->purchase_order_id);
    	return view('admin.transactions.show', compact('transactions', 'title','purchaseorder'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class IndexController extends Controller
{
    public function index()
    {
    	$new_collection = Category::where('name','New Collection')->first();
    	$perfect = Category::where('name','Perfect Style')->first();
    	$every = Category::where('name','Every Day Use')->first();

    	$products = Product::get()->random(3);

    	return view('index',compact('products','new_collection','perfect','every'));
    }
}

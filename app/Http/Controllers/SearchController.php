<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class SearchController extends Controller
{
    public function show(Request $request)
    {

    	$key = $request->search;
    	if(!is_null($key)){

	    	$products = Product::where('name','like','%'.$key.'%')->orWhere('description','like','%'.$key.'%')->get(); 
	    	$message="Results for your search '$key' are:";
	    	
    		if(count($products)==0){
    			$products = Product::get();
                $categories = Category::whereNotNull('parent_category_id')->get(); 
    			$message = "Sorry, we could not found any matching product with your '$key'."; 
    			return view('products.search',compact('products','message','categories'));
    		}
    	}else{
    		$products = Product::get();
    	}

        $categoryIds = [];
        foreach ($products as $product) {
            foreach ($product->categories as $category) {
                if ($category->parent_category_id) {
                    array_push($categoryIds, $category->id);
                }
            }
        }

        $categoriesResult = Category::find($categoryIds);
        $categories = [];
        foreach ($categoriesResult as $category) {
            array_push($categories, [
                'path' => $category->findslug($category->parent_category_id)->first()->slug . '/' . $category->slug,
                'name' => $category->name
            ]);
        }

    	return view('products.search',compact('products','message', 'categories'));
    }
}

<?php

namespace App\Http\Controllers\Session;

use Session;

class ShoppingCart
{
	public $items;

	/**
	 * ShoppingCart constructor
	 *
	 * it initializes the shopping_cart in the session if not initialized
	 */
	public function __construct()
	{
		if (!Session::has('shopping_cart'])) {
			Session::put('shopping_cart', array());
		}

		$this->items = &$_SESSION['shopping_cart'];
	}

    /**
	 * Adds a product to the shopping cart
	 * 
	 * @param array $item [to be added in the shopping cart]
	 * @return void
	 */
	public function addItem($item)
	{
		if (isset($this->items[$item['product_id']])) {
			$item['quantity'] += $this->items[$item['product_id']]['quantity'];
		}
		$this->items[$item['product_id']] = $item;

		setFlashMessage('success','Product has been added to shopping cart');
	}
}

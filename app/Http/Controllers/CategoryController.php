<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{

	/**
	 * This function will search for every parent category to be
	 * displayed on the main navigation
	 *
	 * This means that if a category doesn't have a parent category, it's not a sub category of another one
	 *
	 * We recommend to have a maximum of 6 active categories in DB
	 * so it won't break the layout of the page
	 * @return Array of Category
	 */
    public static function findActive()
    {
    	return Category::whereNull('parent_category_id')->limit(6)->get();
    }


}

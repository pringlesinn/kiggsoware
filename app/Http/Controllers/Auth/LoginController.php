<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Redirecting to specific path based on Role(Admin/user)
     * @return path
     */
    protected function redirectTo()
    {
        if(Auth::user()){
            $role = Auth::user()->is_admin;

            if ($role == 0 ) {
                return (strpos($_SERVER['HTTP_REFERER'], 'checkout') != -1) ? '/checkout' : '/profile';
            }else{
                return '/admin/users';
            }
        }    
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}

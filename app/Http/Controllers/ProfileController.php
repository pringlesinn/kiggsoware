<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\PurchaseOrder;
use App\UserAddress;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
    	if(Auth::user()){
    		$user = Auth::user();
    		$orders = PurchaseOrder::where('user_id',$user->id)->get();
    		return view('profile.index',compact('user','orders'));	
    	}
    }
    public function edit()
    {
    	if(Auth::user()){
    		$user= Auth::user();
            if (count($user->userAddresses) > 1) {
                $user_address = $user->userAddresses->first();    
            } else {
                $user_address = new UserAddress();
                $user_address->street = '';
                $user_address->city = '';
                $user_address->postal_code = '';
                $user_address->country = '';
            }
            
    		return view('profile.edit',compact('user', 'user_address'));
    	}
    }

    public function update(){

    	if(Auth::user()){
    		$user= Auth::user();
    		 $valid = request()->validate([
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'phone' => 'required|regex:/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/',
            'street' => 'required|max:191',
            'city' => 'required|max:191',
            'postal_code' => 'required|max:191|regex:/^([a-zA-Z]{1}\d{1}[a-zA-Z]{1})\s?(\d{1}[a-zA-Z]{1}\d{1})$/',
            'province' => 'required|max:191'
        	]);
	
	        $user->first_name = $valid['first_name'];
	        $user->last_name = $valid['last_name'];
	        $user->phone = $valid['phone'];
	        $user->updated_at = Carbon::now();

            $user_address = (count($user->userAddresses) > 1) ? $user->userAddresses->first() : new UserAddress();

            $user_address->user_id=$user->id;
	        $user_address->street=$valid['street'];
	        $user_address->city=$valid['city'];
            $user_address->province=$valid['province'];
	        $user_address->postal_code=str_replace(' ', '', $valid['postal_code']);
	        $user_address->country='Canada';
            $user_address->updated_at=Carbon::now();

            if ($user_address->id) {
	           $user_address->update();
            } else {
                $user_address->save();
            }

	        if($user_address){
		        $flashMessage = Helper::generateFlashMessage($user->update(), 'update', 'user');

		        return redirect('/profile')->with($flashMessage['type'], $flashMessage['message']);
		    }else{

		    	$flashMessage['type']="error";
		    	$flashMessage['message']="There was a problem in updating information";
		    	return redirect('/profile')->with($flashMessage['type'], $flashMessage['message']);
		    }    


    	}
    }	
}

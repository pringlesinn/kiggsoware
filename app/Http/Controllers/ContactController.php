<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\SendMailable;

use Mail;

class ContactController extends Controller
{
    public function create()
    {
        return view('contact.create');
    }

  public function store(Request $request)
  {

    $valid = request()->validate([
        "firstName" => 'required|max:191',
        "lastName" => 'required|max:191',
        "email" => 'required|email|max:191|regex:/[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/',
        "message" => 'required'
    ]);

    $contact = [];

    $contact['firstName'] = $request->get('firstName');
    $contact['lastName'] = $request->get('lastName');   
    $contact['email'] = $request->get('email');
    $contact['message'] = $request->get('message');

    $contact['to'] = 'contact@kiggsoware.com';

    Mail::to('contact@kiggsoware.com')->send(new SendMailable('emails.contact', 'Contact', $contact));

    return redirect()->route('contact.create');
}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($parentCategorySlug, $subcategorySlug, $id)
    {
        $product = Product::findOrFail($id);
        $title = 'Kiggs O Ware - Product Detail Page';
        $maxQuantity = $product->quantity > 10 ? 10 : $product->quantity;
        
        return view('products.show', compact('product', 'title', 'maxQuantity', 'parentCategorySlug', 'subcategorySlug'));
    }
    
	/**
	 * Equivalent of the index method of Product List View that is being filtered by Parent Category(main navigation bar)
	 * @param  String $parentCategorySlug 
	 * @return View
	 */
	public function filterByParentCategory($parentCategorySlug)
    {
    	$products = Product::findByParentCategory($parentCategorySlug);

    	$subcategories = $this->getSubcategories($parentCategorySlug);

        $currentSubcategory = '';
    	return view('products.index', compact('products', 'parentCategorySlug', 'subcategories', 'currentSubcategory'));
    }

    /**
     * Equivalent of the index method of Product List View that is being filtered by Parent Category and Sub Category (main navigation bar)
     * @param  String $parentCategorySlug
     * @param  String $subcategorySlug
     * @return View
     */
    public function filterBySubCategory($parentCategorySlug, $subcategorySlug)
    {
    	$products = Product::findByParentAndSubcategory($parentCategorySlug, $subcategorySlug);
    	$subcategories = $this->getSubcategories($parentCategorySlug);
        $currentSubcategory = $subcategorySlug;

    	return view('products.index', compact('products', 'parentCategorySlug', 'subcategories', 'currentSubcategory'));
    }

    /**
     * Used to retrieve the list of categories to be displayed on left side for filtering
     * @param  String $parentCategorySlug
     * @param  String $categorySlug
     * @return Array of Subcategories
     */
    private function getSubcategories($parentCategorySlug, $categorySlug = null)
    {
    	return Category::findSubcategories(Category::findByParentAndName($parentCategorySlug, $categorySlug));
    }
}

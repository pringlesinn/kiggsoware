<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PurchaseOrder;

class OrderDetailController extends Controller
{
    public function show($id)
    {
    	$user_id = Auth::user()->id;
    	$purchase_orders = PurchaseOrder::where('user_id',$user_id)->where('id',$id)->get();
    	$data= \DB::table('products')->join('product_purchase_order','product_purchase_order.product_id','=','products.id')->select('products.name','product_purchase_order.quantity','product_purchase_order.unit_price')->where('product_purchase_order.purchase_order_id','=',$id)->get();

    	return view('order.show',compact('purchase_orders','data'));
    }
}

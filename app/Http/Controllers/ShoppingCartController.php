<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\ShoppingCart;

class ShoppingCartController extends Controller
{
	public function index()
	{
		$productsArray = ShoppingCart::getShoppingCartProducts();
		$pricing['subtotal'] = ShoppingCart::getTotalWithoutTaxes();
		$pricing['gst-total'] = ShoppingCart::getGSTForAllProducts();
		$pricing['pst-total'] = ShoppingCart::getPSTForAllProducts();
		$pricing['total'] = ShoppingCart::getTotalWithTaxes();

		return view('shopping-cart.index', compact('productsArray', 'pricing'));
	}

    public function addToCart($productId)
    {
    	ShoppingCart::addItem($productId, request('quantity'));

    	return redirect("/shopping-cart");
    }

    public function subtractOne($productId)
    {
    	ShoppingCart::addItem($productId, -1);

    	return redirect("/shopping-cart");
    }

    public function updateCart($productId)
    {
    	ShoppingCart::updateItem($productId, intval(request()->get('quantity')));

    	return redirect("/shopping-cart");
    }

    public function addOne($productId)
    {
    	ShoppingCart::addItem($productId, 1);

    	return redirect("/shopping-cart");
    }

    public function deleteProduct($productId)
    {
    	ShoppingCart::deleteItem($productId);

    	return redirect("/shopping-cart");
    }
}

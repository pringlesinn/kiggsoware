<?php

namespace App\Helper;

use App\Product;
use Session;

class ShoppingCart
{

	// Taxes constants
	const GST = .07;
	const PST = .05;
	
	/**
	 * ShoppingCart constructor
	 *
	 * it initializes the shopping_cart in the session if not initialized
	 */
	public function __construct()
	{
		if (!Session::has('shopping_cart')) {
			Session::put('shopping_cart', array());
		}
	}

    /**
	 * Adds a product to the shopping cart
	 * 
	 * @param integer $productId
	 * @param integer $quantity
	 * @return void
	 */
	public static function addItem($productId, $quantity)
	{
		$item = array(
			'product_id' => $productId,
			'quantity' => $quantity
		);


		if (Session::has('shopping_cart.' . $item['product_id'])) {
			$item['quantity'] += Session::get('shopping_cart')[$item['product_id']]['quantity'];
		}
		$shoppingCart = Session::get('shopping_cart');
		$shoppingCart[$item['product_id']] = $item;

		Session::put('shopping_cart', $shoppingCart);
	}

	/**
	 * Update quantity from product on Shopping Cart
	 * 
	 * @param integer $productId
	 * @param integer $quantity
	 * @return void
	 */
	public static function updateItem($productId, $quantity)
	{
		if (Session::has('shopping_cart.' . $productId)) {
			Session::put('shopping_cart.' . $productId, 
				array(
					'product_id' => $productId, 
					'quantity' => $quantity)
				);
		}

	}

	/**
	 * Deletes product from shopping cart
	 * 
	 * @param  int $productId
	 * @return void
	 */
	public static function deleteItem($productId)
	{
		Session::forget('shopping_cart.' . $productId);

		// setFlashMessage('success','Product was successfully deleted from cart');
	}

	public static function getShoppingCartProducts()
	{
		if (Session::has('shopping_cart')) {
			foreach (Session::get('shopping_cart') as $item) {
				$result[$item['product_id']] = array(
					'product' => Product::find($item['product_id']),
					'quantity' => $item['quantity']
				);
			}
		}

		return isset($result) ? $result : [] ;
	}

	/**
	 * Get total number of items in the shopping cart
	 * 
	 * @return int
	 */
	public static function getCountItems()
	{
		$countItems = 0;
		if (Session::has('shopping_cart')) {
			foreach (Session::get('shopping_cart') as $item) {
				$countItems +=  $item['quantity'];
			}
		}

		return $countItems;
	}

	/**
	 * Get price times quantity from a particular product
	 * 
	 * @param  float $price
	 * @param  int $quantity
	 * @return float
	 */
	public static function getLineTotal($price, $quantity)
	{
		return $price * $quantity;
	}

	/**
	 * Get Price with GST tax
	 * 
	 * @param  float $price
	 * @param  int $quantity
	 * @return float
	 */
	public static function getGst($price, $quantity) 
	{
		return self::getLineTotal($price, $quantity) * static::GST;
	}

	/**
	 * Get Price with PST tax
	 * 
	 * @param  float $price
	 * @param  int $quantity
	 * @return float
	 */
	public static function getPst($price, $quantity) 
	{
		return self::getLineTotal($price, $quantity) * static::PST;
	}

	/**
	 * Gets full price excluding taxes of 
	 * all items in the Shopping Cart (SESSION)
	 * 
	 * @return float
	 */
	public static function getTotalWithoutTaxes()
	{
		$fullPriceWithoutTaxes = 0;
		if (Session::has('shopping_cart')) {
			foreach (Session::get('shopping_cart') as $item) {
				$product = Product::find($item['product_id']);
				$fullPriceWithoutTaxes += self::getLineTotal($product->price, $item['quantity']);
			}
		}

		return self::numberFormat($fullPriceWithoutTaxes);
	}

	/**
	 * Gets PST for all products
	 * 
	 * @return float
	 */
	public static function getPSTForAllProducts()
	{
		return self::numberFormat(self::getPst(self::getTotalWithoutTaxes(), 1));
	}

	/**
	 * Gets GST for all products
	 * 
	 * @return float
	 */
	public static function getGSTForAllProducts()
	{
		return self::numberFormat(self::getGst(self::getTotalWithoutTaxes(), 1));
	}

	/**
	 * Gets full price including taxes of 
	 * all items in the Shopping Cart (SESSION)
	 * 
	 * @return float
	 */
	public static function getTotalWithTaxes()
	{
		$fullPrice = self::getTotalWithoutTaxes();
		$fullPrice += self::getPSTForAllProducts();
		$fullPrice += self::getGSTForAllProducts();
		
		return self::numberFormat($fullPrice);
	}

	/**
	 * Clear shopping cart
	 * 
	 * @return void
	 */
	public static function clearCart()
	{
		Session::forget('shopping_cart');
	}

	/**
	 * Formats price to have 2 decimals
	 * 
	 * @param  float $number
	 * @return float(,2)
	 */
	private static function numberFormat($number) 
	{
		return number_format($number, 2, '.', '');	
	}

}

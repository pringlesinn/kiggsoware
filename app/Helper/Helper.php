<?php 

namespace App\Helper;

class Helper 
{
    /**
     * Creates an object with flash message and type
     * 
     * @param  Object $object
     * @param  String $action
     * @param  String $tableName
     * @return Array<String>
     */
	public static function generateFlashMessage($object, $action, $tableName)
	{
		if ($object) {
    		$flashMessage['message'] = ucwords($tableName) . " was $action" . "d successfully";
    		$flashMessage['type'] = 'success';
    	} else {
    		$flashMessage['message'] = 'Something went wrong. Please try again';
    		$flashMessage['type'] = 'error';
    	}

    	return $flashMessage;
	}
}
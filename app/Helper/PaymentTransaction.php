<?php 

namespace App\Helper;

use Pagerange\Bx\_5bx;

class PaymentTransaction 
{
	const BX_LOGIN = '5664381';
	const BX_KEY = '7ec39e5691fce6d43723fb3d5fb4330e';

	public static function process($paymentInformation)
	{
		$transaction = new _5bx(static::BX_LOGIN, static::BX_KEY);

	    // Replace hard coded values with your own variables
	    $transaction->amount($paymentInformation['amount']); // total sale
	    $transaction->card_num($paymentInformation['card_num']); // credit card number
	    $transaction->exp_date ($paymentInformation['exp_date']); // expiry date month and year (august 2022)
	    $transaction->cvv($paymentInformation['cvv']); // card cvv number
	    $transaction->ref_num($paymentInformation['ref_num']); // your reference or invoice number
	    $transaction->card_type($paymentInformation['card_type']); // card type (visa, mastercard, amex)
	    return $transaction->authorize_and_capture(); // returns JSON object

	}

}


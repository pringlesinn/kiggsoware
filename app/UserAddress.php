<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{

	use SoftDeletes;

	protected $fillable = ['user_id', 'street', 'city', 'postal_code', 'province', 'country'];
	
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $fillable = ['user_name', 'user_email', 'sub_total', 'gst', 'pst', 'total','shipping_address_json','billing_address_json','estimated_delivery',
        'is_delivered','delivery_date','payment_status'];

    public function products()
    {
    	return $this->belongsToMany('App\Product');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    }

}

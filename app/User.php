<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use Notifiable;

	use SoftDeletes;

    protected $fillable = [ 'first_name', 'last_name',
				'email', 'password','phone'];

	public function purchaseOrders()
	{
		return $this->hasMany('App\PurchaseOrder');
	}

	public function userAddresses()
	{
		return $this->hasMany('App\UserAddress');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $fillable = ['purchase_order_id', 'reference_number', 'response_json'];

    public function purchaseOrder()
    {
    	return $this->belongsTo('App\PurchaseOrder');
    }
}

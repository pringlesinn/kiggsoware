<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemovedData extends Model
{
	protected $table= 'removed_datas';
    protected $fillable = ['id','table_name','content_json'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Category;
use DB;

class Product extends Model
{

	use SoftDeletes;

	protected $fillable= ['name','price','quantity','description','supplier_id',
							'category_id','image','is_active','parent_category_id'];

	/**
	 * Find products by the parent category
	 * @param  String $parentCategorySlug
	 * @return Array of Products
	 */
	public static function findByParentCategory($parentCategorySlug)
	{
		$category = Category::findByParentAndName($parentCategorySlug);
		$subcategories = Category::findSubcategories($category);
		$subcategoryIds = [$category->id]; //starts with parent id

		foreach ($subcategories as $subcategory) {
			array_push($subcategoryIds, $subcategory['id']);
		}

		$products = Product::
		from('products')->
		select('products.id', 'products.name', 'products.price', 'products.image', 'c.slug as category_slug')->
		join('category_product as cp', 'products.id', '=', 'cp.product_id')->
		join('categories as c', 'c.id', '=', 'cp.category_id')->
		whereIn('cp.category_id', $subcategoryIds)->
		get();

		return $products;
	}

	/**
	 * Find Products by parent category and subcategory
	 * @param  String $category        Parent Category
	 * @param  String $subcategoryName Sub Category
	 * @return Array of Products
	 */
	public static function findByParentAndSubcategory($category, $subcategoryName)
	{
		$products = [];

		$category = Category::select('id')->
		where('slug', '=', $category)->
		whereNull('parent_category_id')->
		first();
		
		if ($category) {
			$subcategory = Category::select('id')->
			where('parent_category_id', '=', $category->id)->
			where('slug', '=', $subcategoryName)->
			first();

			if ($subcategory) {
				$products = Product::
				from('products')->
				select('products.id', 'products.name', 'products.price', 'products.image', 'c.slug as category_slug')->
				join('category_product as cp', 'products.id', '=', 'cp.product_id')->
				join('categories as c', 'c.id', '=', 'cp.category_id')->
				where('c.id', $subcategory->id)->
				distinct()->
				get();
			}
		}

		return $products;
	}

	public function categories()
	{
		return $this->belongsToMany('App\Category');
	}

	public function carts()
	{
		return $this->belongsToMany('App\Cart');
	}

	public function supplier()
	{
		return $this->belongsTo('App\Supplier');
	}

	public function purchaseOrders()
	{
		return $this->belongsToMany('App\PurchaseOrder');
	}
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $view;
    public $subject;
    public $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($view, $subject, $params)
    {
        $this->view = $view;
        $this->subject = $subject;
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Kiggs \'O \'Ware - ' . $this->subject)->to($this->params['to'])->view($this->view);
    }
}

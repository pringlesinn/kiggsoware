<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['parent_category_id','name','slug','is_active'];

    public static function findByParentAndName($parentCategorySlug, $categorySlug = null)
    {
        $parentCategory = Category::select('id', 'name')->
        whereNull('parent_category_id')->
        where('slug', $parentCategorySlug)->
        first();

        if (is_null($categorySlug)) {
            return $parentCategory;
        }

    	$category = Category::
        select('id', 'name', 'slug')->
        where('slug', $categorySlug)->
        where('parent_category_id', $parentCategory->id)->
        first();

		return $category;
    }

    /**
     * Find Subcategories of a particular parent. Can also be used to find if the category parameter is a subcategory
     * @param  Category $category
     * @return Array of Subcategories
     */
    public static function findSubcategories($category)
    {
        if ($category) {
            $subcategories = Category::
            select('id', 'name', 'slug')->
            where('parent_category_id', $category->id)->
            get();

            return $subcategories->toArray();
        } else {
            return [];
        }
    }

    /**
     * This function will search for every parent category to be
     * displayed on the main navigation
     *
     * This means that if a category doesn't have a parent category, it's not a sub category of another one
     *
     * We recommend to have a maximum of 6 active categories in DB
     * so it won't break the layout of the page
     * @return Array of Category
     */
    public static function findActive()
    {
        return Category::
                whereNull('parent_category_id')->
                where('is_active', true)->
                limit(6)->
                get();
    }

    public function products()
    {
    	return $this->belongsToMany('App\Product');
    }

    public function findslug($id)
    {

        $parent_category= Category::where('id',$id)->whereNull('parent_category_id')->get(); 
        return $parent_category;
    }

}

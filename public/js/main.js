function hamburger(x) {
	x.classList.toggle("change");
	document.getElementById("navigation").classList.toggle("mobilenavigation");
	if (document.getElementById("navigation").classList.contains("slideup")){
			 document.getElementById("navigation").classList.add("slidedown");
			 document.getElementById("navigation").classList.remove("slideup");
	} else{
			 document.getElementById("navigation").classList.add("slideup");
			 document.getElementById("navigation").classList.remove("slidedown");
	}
}
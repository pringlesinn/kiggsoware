<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');


//Contact
Route::get('contact', 'ContactController@create')->name('contact.create');
Route::post('contact', 'ContactController@store')->name('contact.store');

//About Us
Route::get('/about-us', function() {
	return view('about.index');
});

//Shopping Cart
Route::get('/shopping-cart', 'ShoppingCartController@index');
Route::post('/products/addtocart/{productId}', 'ShoppingCartController@addToCart');
Route::post('/shopping-cart/subtractone/{productId}', 'ShoppingCartController@subtractOne');
Route::put('/shopping-cart/update/{productId}', 'ShoppingCartController@updateCart');
Route::post('/shopping-cart/addone/{productId}', 'ShoppingCartController@addOne');
Route::delete('/shopping-cart/delete-product/{productId}', 'ShoppingCartController@deleteProduct');


Route::group(['middleware' => ['auth']], function () {
	//Profile
	Route::get('/profile','ProfileController@index');
	Route::get('/order_details/{id}','OrderDetailController@show');
	Route::get('/profile/edit','ProfileController@edit');
	Route::put('profile/update','ProfileController@update');
	
	//Checkout
	Route::get('/checkout', 'CheckoutController@index');
	Route::post('/checkout', 'CheckoutController@store');
});


//Products
Route::get('/products/category/{parentCategorySlug}/{subcategorySlug}/{id}', 'ProductController@show'); 
Route::get('/products/category/{parentCategorySlug}', 'ProductController@filterByParentCategory');
Route::get('/products/category/{parentCategorySlug}/{subcategorySlug}', 'ProductController@filterBySubCategory');
Route::post('/products','SearchController@show');

//Using Admin Middleware to protect admin from normal user access
Route::group(['middleware' => ['auth', 'admin']], function() {
    
//Admin Categories
Route::get('/admin/categories','Admin\CategoryAdminController@index');
Route::get('/admin/categories/create','Admin\CategoryAdminController@create');
Route::post('/admin/categories','Admin\CategoryAdminController@store');
Route::put('/admin/categories/{category_id}','Admin\CategoryAdminController@update');
Route::get('/admin/categories/{name}','Admin\CategoryAdminController@edit');
Route::delete('/admin/categories/{id}','Admin\CategoryAdminController@destroy');

//Admin Products
Route::get('/admin/products','Admin\ProductAdminController@index');
Route::get('/admin/products/create','Admin\ProductAdminController@create');
Route::get('/admin/products/{id}','Admin\ProductAdminController@edit');
Route::post('/admin/products','Admin\ProductAdminController@store');
Route::put('/admin/products/{id}','Admin\ProductAdminController@update');
Route::delete('/admin/products/{id}','Admin\ProductAdminController@destroy');
Route::post('/admin/products/get/sub_categories','Admin\ProductAdminController@categories');
Route::post('/admin/products/update/sub_categories','Admin\ProductAdminController@categories');

//Admin Users
Route::get('/admin/users', 'Admin\UserAdminController@index');
Route::get('/admin/users/create', 'Admin\UserAdminController@create');
Route::post('/admin/users', 'Admin\UserAdminController@store');
Route::get('/admin/users/reset-password/{id}', 'Admin\UserAdminController@resetPassword');
Route::get('/admin/users/{user}', 'Admin\UserAdminController@edit');
Route::put('/admin/users/{id}', 'Admin\UserAdminController@update');
Route::delete('/admin/users/{id}', 'Admin\UserAdminController@destroy');

//Admin Purchase Orders
Route::get('/admin/purchase_orders', 'Admin\PurchaseOrderAdminController@index');
Route::get('/admin/purchase_orders/{purchase_order_id}', 'Admin\PurchaseOrderAdminController@show');
Route::get('/admin/purchase_orders/update/{id}','Admin\PurchaseOrderAdminController@edit');
Route::put('/admin/purchase_orders/{id}','Admin\PurchaseOrderAdminController@update');
Route::get('/admin/purchase_orders/{id}/{status}','Admin\PurchaseOrderAdminController@status');


//Admin Suppliers
Route::get('/admin/suppliers', 'Admin\SupplierAdminController@index');
Route::get('/admin/suppliers/create', 'Admin\SupplierAdminController@create');
Route::post('/admin/suppliers', 'Admin\SupplierAdminController@store');
Route::put('/admin/suppliers/{id}', 'Admin\SupplierAdminController@update');
Route::get('/admin/suppliers/{id}/edit', 'Admin\SupplierAdminController@edit');
Route::delete('/admin/suppliers/{id}', 'Admin\SupplierAdminController@destroy');


//Admin Sub-categories 
Route::get('/admin/sub_categories','Admin\SubCategoryAdminController@index');
Route::get('/admin/sub_categories/create','Admin\SubCategoryAdminController@create');
Route::post('/admin/sub_categories','Admin\SubCategoryAdminController@store');
Route::get('/admin/sub_categories/{id}','Admin\SubCategoryAdminController@edit');
Route::put('/admin/sub_categories/{id}','Admin\SubCategoryAdminController@update');
Route::delete('/admin/sub_categories/{id}','Admin\SubCategoryAdminController@destroy');

//Admin Transaction
Route::get('/admin/transactions', 'Admin\TransactionAdminController@index');
Route::get('/admin/transactions/{id}', 'Admin\TransactionAdminController@show');

});

//For auth(Regsiter and Login)
Auth::routes();
Route::get('/home', 'HomeController@index')->name('/');
Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('login');
});




